<?php
namespace Tickets4Sale\Infrastructure\Ticket;

use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Ticket\Ticket;
use Tickets4Sale\Domain\Ticket\TicketRepository;
use Tickets4Sale\Infrastructure\Repository\RepositoryWrapper;
use ArrayIterator;
use DateTimeImmutable;

/**
 * Class DoctrineTicketRepository
 * Our own implementation of TicketRepository, using external dependency for DBAL through a RepositoryWrapper.
 *
 * @package Tickets4Sale\Infrastructure\Ticket
 */
class DoctrineTicketRepository implements TicketRepository
{
    /**
     * @var RepositoryWrapper
     */
    private $repositoryWrapper;

    /**
     * DoctrineTicketRepository constructor.
     *
     * @param RepositoryWrapper $repositoryWrapper
     */
    public function __construct(RepositoryWrapper $repositoryWrapper)
    {
        $this->repositoryWrapper = $repositoryWrapper;
    }

    /**
     * @param Ticket $ticket
     */
    public function save(Ticket $ticket): void
    {
        $this->repositoryWrapper->save($ticket);
    }

    /**
     * @param string $id
     * @return null|Ticket
     */
    public function find(string $id):? Ticket
    {
        return $this->repositoryWrapper->find($id);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     * @return null|ArrayIterator
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ):? ArrayIterator {
        $records = $this->repositoryWrapper->findBy($criteria, $orderBy, $limit, $offset);
        if (!is_null($records)) {
            return $records->getIterator();
        }
        return new ArrayIterator();
    }

    /**
     * Counts Tickets sold for a given Show and Show date
     * @param Show $show
     * @param DateTimeImmutable $showDate
     * @return int
     */
    public function countSoldTickets(Show $show, \DateTimeImmutable $showDate): int
    {
        return $this->repositoryWrapper->countBy(['show' => $show, 'showDate' => $showDate]);
    }
}
