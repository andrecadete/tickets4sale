<?php
namespace Tickets4Sale\Application\Service;

use Tickets4Sale\Application\Show\ShowInventoryAccessRequest;
use Tickets4Sale\Application\Show\ShowInventoryAccessResponse;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use Tickets4Sale\Application\Show\ShowInventoryStatusResponse;

/**
 * Interface ShowInventoryService
 *
 * @package Tickets4Sale\Application\Service
 */
interface ShowInventoryService
{
    /**
     * Runs business logic on Shows to discern their current statuses
     *
     * @param  ShowInventoryStatusRequest $request
     * @return ShowInventoryStatusResponse
     */
    public function checkInventoryStatus(ShowInventoryStatusRequest $request): ShowInventoryStatusResponse;

    /**
     * Reads the whole available Show inventory based on a show date provided in the request
     *
     * @param  ShowInventoryAccessRequest $request
     * @return ShowInventoryAccessResponse
     */
    public function readInventory(ShowInventoryAccessRequest $request): ShowInventoryAccessResponse;
}
