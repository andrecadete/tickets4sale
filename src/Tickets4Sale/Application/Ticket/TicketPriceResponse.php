<?php
namespace Tickets4Sale\Application\Ticket;

/**
 * Class TicketPriceResponse
 *
 * @package Tickets4Sale\Application\Ticket
 */
class TicketPriceResponse
{
    /**
     * @var int
     */
    private $price;

    /**
     * TicketPriceResponse constructor.
     *
     * @param float $price
     */
    public function __construct(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function cost(): float
    {
        return $this->price;
    }
}
