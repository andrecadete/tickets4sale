<?php
namespace Tickets4Sale\Application\Http\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tickets4Sale\Application\Show\ShowInventoryAccessRequest;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use Tickets4Sale\Application\Service\ShowInventoryService;
use DateTimeImmutable;

/**
 * Class DashboardController
 *
 * @package Tickets4Sale\Application\Http\Controller
 */
class DashboardController extends Controller
{

    /**
     * @var ShowInventoryService $inventoryService
     */
    private $inventoryService;

    public function __construct(ShowInventoryService $inventoryService)
    {
        $this->inventoryService = $inventoryService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function getIndex(Request $request): Response
    {
        // by default check today's availability
        $chosenDate = $request->get('show_date') ? $request->get('show_date') : date('Y-m-d');
        $desiredShowDate = (new DateTimeImmutable())::createFromFormat('Y-m-d', $chosenDate)->setTime(0, 0, 0);
        $updatedInventory = [];
        $now = (new DateTimeImmutable())->setTime(0, 0, 0);

        if ($desiredShowDate) {
            $inventory = $this->inventoryService->readInventory(
                new ShowInventoryAccessRequest($desiredShowDate)
            )->access();

            $updatedInventory = $this->inventoryService->checkInventoryStatus(
                new ShowInventoryStatusRequest($inventory, $now, $desiredShowDate)
            )->showInventory();
        } else {
            $this->addFlash('error', 'The chosen date is not a valid ISO 8601 date (yyyy-mm-dd)');
        }
        return $this->render('dashboard/main.html.twig', ['inventory' => $updatedInventory]);
    }
}
