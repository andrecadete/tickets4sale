<?php
namespace Tickets4Sale\Application\Ticket;

use Tickets4Sale\Domain\Ticket\Ticket;

/**
 * Class PurchaseTicketResponse
 *
 * @package Tickets4Sale\Application\Ticket
 */
class PurchaseTicketResponse
{
    /**
     * @var Ticket $ticket
     */
    private $ticket;

    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    public function readTicket(): Ticket
    {
        return $this->ticket;
    }
}
