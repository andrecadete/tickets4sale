<?php

namespace Tickets4Sale\Domain\Genre;

/**
 * Class Musical
 *
 * @package Tickets4Sale\Domain\Genre
 */
class Musical extends Genre
{
    protected static $price = 70;
}
