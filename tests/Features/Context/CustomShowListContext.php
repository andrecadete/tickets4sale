<?php
namespace Tickets4Sale\Tests\Features\Context;

use Behat\Behat\Context\Context;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpKernel\KernelInterface;
use Tickets4Sale\Application\Console\CustomShowListTicketStatusCommand;

/**
 * Class CustomShowListContext
 * @package Tickets4Sale\Tests\Features\Context
 */
class CustomShowListContext extends TestCase implements Context
{
    /**
     * @var KernelInterface $kernel
     */
    protected $kernel;

    /**
     * @var Application $consoleApplication
     */
    protected $consoleApplication;

    /**
     * @var Command $command
     */
    private $command;

    /**
     * @var string $fileName
     */
    private $fileName;

    /**
     * @var string $commandOutput
     */
    private $commandOutput;

    /**
     * CustomShowListContext constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->consoleApplication = new Application($this->kernel);
        $this->consoleApplication->setAutoExit(false);
        $this->fileName = null;
        $this->commandOutput = '';
        $this->command = $this->consoleApplication->find('tickets4sale:custom-inventory:status');
        parent::__construct($this->consoleApplication);
    }

    /**
     * @Given the custom-list ticket inventory tool
     */
    public function theCustomListTicketInventoryTool()
    {
        $this->assertInstanceOf(CustomShowListTicketStatusCommand::class, $this->command);
    }

    /**
     * @Given a CSV File :arg1
     *
     * @param string $arg1
     */
    public function aCsvFile(string $arg1)
    {
        $this->assertFileExists(
            $this->kernel->getRootDir() . '/../' . $arg1,
            'File could not be found to continue the test!'
        );
        $this->assertContains('.csv', $arg1);
        $this->fileName = $arg1;
    }

    /**
     * @When the custom-list ticket inventory tool is executed with the parameters :arg1 and :arg2
     * @param string $arg1
     * @param string $arg2
     */
    public function theCustomListTicketInventoryToolIsExecutedWithTheParametersAnd(string $arg1, string $arg2)
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute(['file-path' => $this->fileName, 'query-date' => $arg1, 'show-date' => $arg2]);

        $this->commandOutput = $commandTester->getDisplay();
    }

    /**
     * @Then there are :arg1 Tickets remaining, :arg2 Tickets available and the status is ":arg3" for the show ":arg4"
     */
    public function thereAreTicketsRemainingTicketsAvailableAndTheStatusIsForTheShow($arg1, $arg2, $arg3, $arg4)
    {
        $found = false;
        $readableOutput = json_decode($this->commandOutput, true);
        foreach ($readableOutput['inventory'] as $genreGroup) {
            foreach ($genreGroup['shows'] as $show) {
                if (strtolower($show['title']) === strtolower($arg4)) {
                    $this->assertEquals($arg1, $show['tickets_left']);
                    $this->assertEquals($arg2, $show['tickets_available']);
                    $this->assertEquals(strtolower($arg3), strtolower($show['status']));
                    $found = true;
                }
            }
        }

        if (!$found) {
            throw new \Exception('The show name in the example table does not exist in the provided file');
        }
    }



}
