<?php
namespace Tickets4Sale\Infrastructure\Repository;

use Doctrine\Common\Collections\Collection;
use Tickets4Sale\Domain\Entity\Entity;
use DateTimeInterface;

/**
 * Interface Repository
 * Abstraction layer for our very own Repository Classes.
 *
 * @package Tickets4Sale\Infrastructure\Repository
 */
interface RepositoryWrapper
{
    /**
     * Finds one Entity by Id
     *
     * @param  string $id
     * @return null|Entity
     */
    public function find(string $id):? Entity;

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return mixed
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null):? Collection;

    /**
     * @param Entity $entity
     */
    public function save(Entity $entity): void;

    /**
     * Finds rows where a given DateTime $value is between the value of $greaterThanField and $smallerThanField.
     *
     * @param  DateTimeInterface $value
     * @param  string            $greaterThanField
     * @param  string            $smallerThanField
     * @param  int|null          $limit
     * @return array
     */
    public function findByValueBetweenDateFields(
        DateTimeInterface $value,
        string $greaterThanField,
        string $smallerThanField,
        ?int $limit = 0
    ): array;

    /**
     * Counts amount of rows that matches given criteria
     *
     * @param array $criteria
     * @return int
     */
    public function countBy(array $criteria = []): int;
}
