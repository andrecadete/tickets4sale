<?php
namespace Tickets4Sale\Tests\Unit\Application\Inventory;

use PHPUnit\Framework\TestCase;
use Tickets4Sale\Application\Show\ShowInventoryService;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use Tickets4Sale\Application\Show\ShowInventoryStatusResponse;
use Tickets4Sale\Application\Service\ShowInventoryService as ShowInventoryServiceContract;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowRepository;
use Tickets4Sale\Domain\Show\ShowStatusService;
use Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService;
use PHPUnit_Framework_MockObject_MockObject;
use Tickets4Sale\Domain\Ticket\TicketPriceService;
use Tickets4Sale\Domain\Ticket\TicketRepository;

/**
 * Class ShowInventoryServiceTest
 * @package Tickets4Sale\Tests\Unit\Application\Inventory
 */
class ShowInventoryServiceTest extends TestCase
{
    /**
     * @var ShowInventoryServiceContract
     */
    private $inventoryService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $showStatusService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $maximumAvailabilityTicketService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $showRepository;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject
     */
    private $ticketRepository;

    public function setUp()
    {
        $this->maximumAvailabilityTicketService = $this->getMockBuilder(TicketMaximumAvailabilityService::class)
            ->getMock();
        $this->showStatusService = $this->getMockBuilder(ShowStatusService::class)->getMock();

        $this->showRepository = $this->getMockBuilder(ShowRepository::class)->disableOriginalConstructor()->getMock();

        $this->ticketRepository = $this->getMockBuilder(TicketRepository::class)->disableOriginalConstructor()
            ->getMock();

        $this->ticketPriceService = $this->getMockBuilder(TicketPriceService::class)->getMock();

        $this->inventoryService = new ShowInventoryService(
            $this->maximumAvailabilityTicketService,
            $this->showStatusService,
            $this->ticketPriceService,
            $this->showRepository,
            $this->ticketRepository
        );
    }

    /**
     * @test
     */
    public function itChecksHowManyTicketsLeftForStatusOpen()
    {
        $request = $this->getMockBuilder(ShowInventoryStatusRequest::class)->disableOriginalConstructor()
            ->getMock();
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();

        // 2 shows in request
        $request->expects($this->once())->method('whichShows')->willReturn([$show, $show]);

        // which means we must get status for a show twice
        $this->showStatusService->expects($this->exactly(2))->method('checkStatus')
            ->willReturn(Show::STATUS_SALES_OPEN);

        $this->ticketRepository->expects($this->exactly(2))->method('countSoldTickets');

        // which if "Open sales", the available tickets (daily and overall) must be fetched
        $this->maximumAvailabilityTicketService->expects($this->exactly(2))->method('totalMaximumAvailable');
        $this->maximumAvailabilityTicketService->expects($this->exactly(2))->method('dailyMaximumAvailable');
        $response = $this->inventoryService->checkInventoryStatus($request);

        $this->assertInstanceOf(ShowInventoryStatusResponse::class, $response);

        $this->assertCount(2, $response->showInventory()[0]['shows']);
    }
}
