<?php
namespace Tickets4Sale\Domain\Show;

use LogicException;
use Throwable;

/**
 * Class ShowAlreadyExistsException
 *
 * @package Tickets4Sale\Domain\Show
 */
class ShowAlreadyExistsException extends LogicException
{
    public function __construct(Show $show, $code = 0, Throwable $previous = null)
    {
        $message = 'The Show "' . $show->name() . '"' .
            ' with start-date "' . $show->startsOn()->format('Y-m-d') . '" already exists in the system.';
        parent::__construct($message, $code, $previous);
    }
}