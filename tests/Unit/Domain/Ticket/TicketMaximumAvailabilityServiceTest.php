<?php
namespace Tickets4Sale\Tests\Unit\Domain\Ticket;

use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService;
use Tickets4Sale\Tests\Fixtures\InMemoryFixtureAwareKernelTestCase;
use DateTimeImmutable;
use DateInterval;

/**
 * Class TicketMaximumAvailabilityService
 * Tests that the maximum tickets are available in the first day of Sales and only 5 available on the last day of sales.
 * Doesn't really test a lot but there's not a lot to Unit test here to be fair.
 * Functional tests using PHPUnit or BDD feature tests using Behat will cover all the scenarios.
 *
 * @package Tickets4Sale\Tests\Unit\Domain\Ticket
 */
class TicketMaximumAvailabilityServiceTest extends InMemoryFixtureAwareKernelTestCase
{

    /**
     * @var TicketMaximumAvailabilityService $ticketMaxAvailabilityService
     */
    private $ticketMaxAvailabilityService;

    public function setUp()
    {
        parent::setUp();
        $this->ticketMaxAvailabilityService = new TicketMaximumAvailabilityService();
    }

    /**
     * @test
     */
    public function theMaximumTicketsAndSeatsMatchOnFirstTicketSaleForFirstShow()
    {
        $queryDate = new DateTimeImmutable();
        $firstShowDate = $queryDate->add(new DateInterval('P25D'));
        // a show that starts 25 days from the query date
        // therefore, must have just opened sales at that point in time if trying to buy for the 1st show
        /**
         * @var Show $show
         */
        $show = $this->fixtures->create(Show::class, ['startDate' => $firstShowDate])->first();
        $totalAvailable = $this->ticketMaxAvailabilityService->totalMaximumAvailable($show, $queryDate, $firstShowDate);
        $this->assertEquals(Show::THEATER_SEATS[Show::BIG_HALL], $totalAvailable);
    }

    /**
     * Tests a max of ten tickets available 5 days before the first show which runs on the big hall.
     * @test
     */
    public function maximumTenTicketsAvailableFiveDaysBeforeFirstShow()
    {
        $queryDate = new DateTimeImmutable();
        $firstShowDate = $queryDate->add(new DateInterval('P5D'));

        // a show that starts 6 days from the query date
        // therefore, must have just opened sales at that point in time if trying to buy for the 1st show
        /**
         * @var Show $show
         */
        $show = $this->fixtures->create(Show::class, ['startDate' => $firstShowDate])->first();
        $totalAvailable = $this->ticketMaxAvailabilityService->totalMaximumAvailable($show, $queryDate, $firstShowDate);

        $this->assertEquals(TicketMaximumAvailabilityService::DAILY_AMOUNTS[Show::BIG_HALL], $totalAvailable);
    }

    /**
     * Tests a max of 5 tickets available at the last ticket-sales day,
     * which is 6 days before the last show and runs at the small hall
     * @test
     */
    public function maximumFiveTicketsAvailableFiveDaysBeforeLastShow()
    {
        $firstShowDate = new DateTimeImmutable();
        /**
         * @var Show $show
         */
        $show = $this->fixtures->create(Show::class, ['startDate' => $firstShowDate])->first();
        // last sales day
        $queryDate = $show->ticketSalesEndOn();
        // last show
        $indendedShowDate = $show->endsOn();

        $totalAvailable = $this->ticketMaxAvailabilityService->totalMaximumAvailable(
            $show,
            $queryDate,
            $indendedShowDate
        );

        $this->assertEquals(TicketMaximumAvailabilityService::DAILY_AMOUNTS[Show::SMALL_HALL], $totalAvailable);
    }

    /**
     * @test
     */
    public function noTicketsAvailableFourDaysBeforeAShow()
    {
        $queryDate = new DateTimeImmutable();
        $chosenShowDate = $queryDate->add(new DateInterval('P4D'));

        // a show that starts 6 days from the query date
        // therefore, must have just opened sales at that point in time if trying to buy for the 1st show
        /**
         * @var Show $show
         */
        $show = $this->fixtures->create(Show::class, ['startDate' => $chosenShowDate])->first();
        $totalAvailable = $this->ticketMaxAvailabilityService->totalMaximumAvailable($show, $queryDate, $chosenShowDate);

        $this->assertEquals(0, $totalAvailable);
    }

    /**
     * Ensures no calculation of available tickets can ever be greater than the maximum show tickets
     * by checking a show date in the past which must forcefully match the maximum.
     * This test runs one assertion per different Theater
     *
     * @test
     */
    public function theOverallSoldTicketsNeverExceedMaximumTicketsPerShow()
    {
        // the past
        $firstShowDate = new DateTimeImmutable();
        // first big hall show
        $chosenBigHallShowDate = $firstShowDate;
        $queryDate = $firstShowDate->add(new DateInterval('P300D'));
        /**
         * @var Show $show
         */
        $show = $this->fixtures->create(Show::class, ['startDate' => $firstShowDate])->first();

        $soldTickets = $this->ticketMaxAvailabilityService->calculateOverallSoldTicketsForShowDay(
            $show,
            $queryDate,
            $chosenBigHallShowDate
        );

        $this->assertEquals(Show::THEATER_SEATS[Show::BIG_HALL], $soldTickets);


        $soldTicketsSmallHall = $this->ticketMaxAvailabilityService->calculateOverallSoldTicketsForShowDay(
            $show,
            $queryDate,
            $show->endsOn()
        );

        $this->assertEquals(Show::THEATER_SEATS[Show::SMALL_HALL], $soldTicketsSmallHall);
    }
}
