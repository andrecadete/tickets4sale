<?php
namespace Tickets4Sale\Infrastructure\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Tickets4Sale\Domain\Entity\Entity;
use DateTimeInterface;

/**
 * Class DoctrineRepositoryWrapper
 *
 * Wrapper for Doctrine's Repository. Ensures that if Doctrine's internals change, our Application layer won't change
 * since it should call the Wrapper and not the Dependency.
 *
 * @package Tickets4Sale\Infrastructure\Show
 */
class DoctrineRepositoryWrapper implements RepositoryWrapper
{
    /**
     * @var ObjectRepository $repository
     */
    protected $repository;

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var string $entityClassName
     */
    protected $entityClassName;

    /**
     * DoctrineRepositoryWrapper constructor.
     *
     * @param EntityManagerInterface $doctrineEntityManager
     * @param string                 $entityClassName
     */
    public function __construct(EntityManagerInterface $doctrineEntityManager, string $entityClassName)
    {
        $this->em = $doctrineEntityManager;
        $this->repository = $doctrineEntityManager->getRepository($entityClassName);
        $this->entityClassName = $entityClassName;
    }

    /**
     * @param string $id
     * @return null|Entity
     */
    final public function find(string $id):? Entity
    {
        return $this->repository->find($id);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return Collection
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): Collection
    {
        return new ArrayCollection($this->repository->findBy($criteria, $orderBy, $limit, $offset));
    }

    /**
     * @param Entity $entity
     */
    public function save(Entity $entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param DateTimeInterface $value
     * @param string            $greaterThanField
     * @param string            $smallerThanField
     * @param int|null          $limit
     *
     * @return array
     */
    public function findByValueBetweenDateFields(
        DateTimeInterface $value,
        string $smallerThanField,
        string $greaterThanField,
        ?int $limit = 0
    ): array {
        $qb = $this->em->createQueryBuilder();

        $query = $qb->select('a')
            ->from($this->entityClassName, 'a')
            ->where('a.' . $smallerThanField . ' <= :showDate')
            ->andWhere('a.' . $greaterThanField . ' >= :showDate')
            ->setParameter('showDate', $value->format('Y-m-d 00:00:00'));

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Counts amount of rows that matches given criteria
     *
     * @param array $criteria
     * @return int
     */
    public function countBy(array $criteria = []): int
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('COUNT(a)')
            ->from($this->entityClassName, 'a');

        foreach ($criteria as $field => $value) {
            $qb->andWhere('a.' . $field . ' = :' . $field);
            $qb->setParameter($field, $value);
        }

        return (int)$qb->getQuery()->getSingleScalarResult();
    }
}
