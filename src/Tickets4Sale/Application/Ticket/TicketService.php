<?php
namespace Tickets4Sale\Application\Ticket;

use Tickets4Sale\Application\Service\TicketService as TicketServiceContract;
use Tickets4Sale\Domain\Show\SalesClosedException;
use Tickets4Sale\Domain\Show\SalesNotOpenException;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowDoesNotExistException;
use Tickets4Sale\Domain\Show\ShowRepository;
use Tickets4Sale\Domain\Show\ShowStatusService;
use Tickets4Sale\Domain\Ticket\Ticket;
use Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService;
use Tickets4Sale\Domain\Ticket\TicketPriceService;
use Tickets4Sale\Domain\Ticket\TicketRepository;
use DateTimeImmutable;
use Tickets4Sale\Domain\Ticket\TicketsSoldOutException;

class TicketService implements TicketServiceContract
{
    /**
     * @var TicketRepository $ticketRepository
     */
    private $ticketRepository;

    /**
     * @var TicketMaximumAvailabilityService $ticketAvailabilityService
     */
    private $ticketAvailabilityService;

    /**
     * @var TicketPriceService $ticketPriceService
     */
    private $ticketPriceService;

    /**
     * @var ShowStatusService $showStatusService
     */
    private $showStatusService;

    /**
     * @var ShowRepository $showRepository
     */
    private $showRepository;

    /**
     * TicketService constructor.
     *
     * @param TicketRepository $ticketRepository
     * @param TicketMaximumAvailabilityService $ticketAvailabilityService
     * @param TicketPriceService $ticketPriceService
     * @param ShowStatusService $showStatusService
     * @param ShowRepository $showRepository
     */
    public function __construct(
        TicketRepository $ticketRepository,
        TicketMaximumAvailabilityService $ticketAvailabilityService,
        TicketPriceService $ticketPriceService,
        ShowStatusService $showStatusService,
        ShowRepository $showRepository
    ) {
        $this->ticketRepository = $ticketRepository;
        $this->ticketAvailabilityService = $ticketAvailabilityService;
        $this->ticketPriceService = $ticketPriceService;
        $this->showStatusService = $showStatusService;
        $this->showRepository = $showRepository;
    }

    /**
     * @param PurchaseTicketRequest $request
     * @return PurchaseTicketResponse
     *
     * @throws SalesClosedException
     * @throws SalesNotOpenException
     * @throws TicketsSoldOutException
     * @throws ShowDoesNotExistException
     */
    public function purchaseTicket(PurchaseTicketRequest $request): PurchaseTicketResponse
    {
        // first make sure this show exists
        $show = $this->showRepository->findBy(['name' => $request->whichShow()], null, 1);

        if ($show && $show->count()) {
            $show = $show->current();
            // first check if sales status allow a purchase
            $this->validateSalesStatus($show, $request->when());

            // check if we're allowed to sell any tickets for the desired show today
            $maximumDailyTickets = $this->ticketAvailabilityService->dailyMaximumAvailable(
                $show,
                $request->when()
            );

            // now check how many were sold for that day already
            $soldTickets = $this->ticketRepository->countSoldTickets($show, $request->when());
            $availableTickets = $maximumDailyTickets - $soldTickets;

            if ($availableTickets < 1) {
                throw new TicketsSoldOutException($show->name());
            }

            // finally get the price
            $price = $this->ticketPriceService->checkPrice($show, $request->when());

            // issue and purchase ticket
            $ticket = new Ticket(null, $request->when(), $show, $price);
            $ticket->purchase();

            $this->ticketRepository->save($ticket);

            return new PurchaseTicketResponse($ticket);
        }

        throw new ShowDoesNotExistException($request->whichShow());
    }

    /**
     * @param TicketPriceRequest $request
     * @return TicketPriceResponse
     */
    public function checkPrice(TicketPriceRequest $request): TicketPriceResponse
    {
        $price = $this->ticketPriceService->checkPrice($request->whichShow(), $request->when());
        return new TicketPriceResponse($price);
    }

    /**
     * @param $show
     * @param $showDate
     * @throws SalesClosedException
     * @throws SalesNotOpenException
     */
    private function validateSalesStatus(Show $show, DateTimeImmutable $showDate): void
    {
        $salesStatus = $this->showStatusService->checkStatus(
            $show,
            new DateTimeImmutable(),
            $showDate
        );

        switch ($salesStatus) {
            case Show::STATUS_SALES_NOT_STARTED:
                throw new SalesNotOpenException($show->ticketSalesStartOn($showDate));
                break;
            case Show::STATUS_SALES_SOLD_OUT:
                throw new TicketsSoldOutException($show->name());
                break;
            case Show::STATUS_SALES_CLOSED:
                throw new SalesClosedException($show->ticketSalesEndOn($showDate));
                break;
        }
    }
}
