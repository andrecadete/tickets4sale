<?php
namespace Tickets4Sale\Application\Show;

use DateTimeImmutable;

/**
 * Class class ShowInventoryAccessRequest
 *
 * @package Tickets4Sale\Application\Show
 */
class ShowInventoryAccessRequest
{
    /**
     * @var array $showDate
     */
    private $showDate;

    /**
     * InventoryAccessRequest constructor.
     *
     * @param DateTimeImmutable $showDate
     */
    public function __construct(DateTimeImmutable $showDate)
    {
        $this->showDate = $showDate;
    }
    /**
     * @return DateTimeImmutable
     */
    public function when(): DateTimeImmutable
    {
        return $this->showDate;
    }
}
