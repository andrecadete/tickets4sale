# System Requirements

### PHP 7.1 and extensions for Symfony

```
sudo apt install php7.1 php7.1-cli php7.1-common php7.1-json php7.1-opcache php7.1-mysql php7.1-mbstring php7.1-mcrypt php7.1-fpm php7.1-xml
# or homebrew equivalent
```

### MySQL or MariaDB server & MySQL client

```
sudo apt install mariadb-server # or mysql-server
sudo apt install mysql
# or homebrew equivalent (if you're running a Mac, I'd strongly suggest you forget about mariadb - it's tricky to get mariadb server + mysql client, easier to have mysql + mysql-server)
```

### composer

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

# Installation

### Clone this Repo and get in there!

```
git clone git@bitbucket.org:andrecadete/tickets4sale.git
cd tickets4sale
```

### Create databases and users (default and test)
```
mysql -u root -pYOUR_ROOT_PASSWORD < initial.sql
```

### Install dependencies
```
composer install
``` 

### Create schema (default and test)
```
bin/console doctrine:schema:create
bin/console doctrine:schema:create --em=test
```

# Starting up

### For simplicty just start-up PHP7's built in server (point to your preferred unused port)
```
php -S 127.0.0.1:8000 -t public/
```

### The minimalistic Show status Dashboard web-page is running on the root route ("127.0.0.1:8000/")

#### (Optional) Import initial data (provided shows.csv) into Database, to view in webpage and/or purchase tickets from

```
# bin/console tickets4sale:show:import path/to/file; Example:
bin/console tickets4sale:show:import tests/data/shows.csv
```

#### (Optional) Import a custom list into memory to check statuses against, simulating the query-date (manual testing I suppose)

```
# bin/console tickets4sale:custom-inventory:status path/to/file query-date show-date; Example:
bin/console tickets4sale:custom-inventory:status tests/data/story1.csv 2017-01-01 2017-07-01 
```

#### (Optional) Quickly get the current live inventory for a single show-date

```
# bin/console tickets4sale:imported-inventory:status show-date; Example:
bin/console tickets4sale:imported-inventory:status 2018-02-28
```

#### (Optional) Purchase a Ticket for a live inventory Show, based on a Show name and date

```
# don't forget to quote the name, unless it's spaceless.
# bin/console tickets4sale:ticket:purchase "show-name" show-date; Example:
bin/console tickets4sale:ticket:purchase "ELF THE MUSICAL" 2018-02-28 

# If a Ticket can't be purchased, you'll see the reason in the terminal.
```

### NOTE: all "path/to/file" occurrences are relative to the project root path


# Testing


### PHPUnit tests - Mostly Unit tests, some Functional
```
bin/test
# OR

bin/test --testsuite=unit
bin/test --testsuite=functional
```

### Behat Feature tests - Functional / Integration
```
bin/behat
```