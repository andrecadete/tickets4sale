<?php
namespace Tickets4Sale\Domain\ValueObject;

/**
 * Interface ValueObject
 *
 * @package Tickets4Sale\Domain\ValueObject
 */
interface ValueObject
{
    /**
     * Value objects should have a string representation
     *
     * @return string
     */
    public function __toString(): string;
}
