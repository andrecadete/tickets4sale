<?php
namespace Tickets4Sale\Application\Show;

use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowRepository;
use Tickets4Sale\Domain\Show\ShowStatusService;
use Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService;
use Tickets4Sale\Application\Service\ShowInventoryService as ShowInventoryServiceContract;
use DateTimeImmutable;
use Tickets4Sale\Domain\Ticket\TicketPriceService;
use Tickets4Sale\Domain\Ticket\TicketRepository;

/**
 * Class ShowInventoryService
 *
 * @package Tickets4Sale\Application\Inventory
 */
class ShowInventoryService implements ShowInventoryServiceContract
{
    const VISUAL_STATUSES = [
        Show::STATUS_SALES_NOT_STARTED => 'Sales not started',
        Show::STATUS_SALES_OPEN => 'Open for sale',
        Show::STATUS_SALES_CLOSED => 'In the Past',
        Show::STATUS_SALES_SOLD_OUT => 'Sold out',
    ];

    /**
     * @var TicketMaximumAvailabilityService $ticketMaxAvailabilityService
     */
    private $ticketMaxAvailabilityService;

    /**
     * @var ShowStatusService $ShowStatusService
     */
    private $showStatusService;

    /**
     * @var ShowRepository $showRepository
     */
    private $showRepository;

    /**
     * @var TicketRepository
     */
    private $ticketRepository;

    /**
     * @var TicketPriceService
     */
    private $ticketPriceService;

    /**
     * ShowInventoryService constructor.
     *
     * @param TicketMaximumAvailabilityService $ticketMaxAvailabilityService
     * @param ShowStatusService $ShowStatusService
     * @param TicketPriceService $ticketPriceService
     * @param ShowRepository $showRepository
     * @param TicketRepository $ticketRepository
     */
    public function __construct(
        TicketMaximumAvailabilityService $ticketMaxAvailabilityService,
        ShowStatusService $ShowStatusService,
        TicketPriceService $ticketPriceService,
        ShowRepository $showRepository,
        TicketRepository $ticketRepository
    ) {
        $this->ticketMaxAvailabilityService = $ticketMaxAvailabilityService;
        $this->showStatusService = $ShowStatusService;
        $this->showRepository = $showRepository;
        $this->ticketRepository = $ticketRepository;
        $this->ticketPriceService = $ticketPriceService;
    }

    /**
     * @param ShowInventoryAccessRequest $request
     * @return ShowInventoryAccessResponse
     */
    public function readInventory(ShowInventoryAccessRequest $request): ShowInventoryAccessResponse
    {
        return new ShowInventoryAccessResponse($this->showRepository->findByShowDate($request->when()));
    }

    /**
     * @param ShowInventoryStatusRequest $request
     * @return ShowInventoryStatusResponse
     */
    public function checkInventoryStatus(ShowInventoryStatusRequest $request): ShowInventoryStatusResponse
    {
        $inventoryStatus = [];
        $perGenreInventory = $this->fillShowInfo($request->whichShows(), $request->queriedOn(), $request->when());

        // reorganize the array as desired for output
        if (!empty($perGenreInventory)) {
            foreach (array_keys($perGenreInventory) as $genre) {
                $inventoryStatus[] = ['genre' => $genre, 'shows' => $perGenreInventory[$genre]['shows']];
                unset($perGenreInventory[$genre]['shows']);
            }
        }


        return new ShowInventoryStatusResponse($inventoryStatus, $request->queriedOn(), $request->when());
    }

    /**
     * @param array             $shows
     * @param DateTimeImmutable $queryDate
     * @param DateTimeImmutable $showDate
     *
     * @return array
     */
    private function fillShowInfo(array $shows, DateTimeImmutable $queryDate, DateTimeImmutable $showDate): array
    {
        $perGenreInventory = [];
        // check if sales are open
        foreach ($shows as $show) {
            $perGenreInventory[(string)$show->whichGenre()]['shows'][] = $this->buildShowInfo(
                $show,
                $queryDate,
                $showDate
            );
        }

        return $perGenreInventory;
    }

    /**
     * @param Show              $show
     * @param DateTimeImmutable $queryDate
     * @param DateTimeImmutable $showDate
     *
     * @return array
     */
    private function buildShowInfo(Show $show, DateTimeImmutable $queryDate, DateTimeImmutable $showDate)
    {
        $showStatus = $this->showStatusService->checkStatus($show, $queryDate, $showDate);
        $ticketsAvailable = 0;
        $ticketsLeft = 0;
        $ticketsSold = 0;

        switch ($showStatus) {
            case Show::STATUS_SALES_NOT_STARTED:
                $ticketsLeft = 200;
                $ticketsAvailable = 0;
                break;
            case Show::STATUS_SALES_OPEN:
                $ticketsLeft = $this->ticketMaxAvailabilityService->totalMaximumAvailable(
                    $show,
                    $queryDate,
                    $showDate
                );
                $ticketsAvailable = $this->ticketMaxAvailabilityService->dailyMaximumAvailable(
                    $show,
                    $showDate
                );
                $ticketsSold = $this->ticketRepository->countSoldTickets($show, $showDate);
                if ($ticketsSold === $ticketsAvailable) {
                    $showStatus = Show::STATUS_SALES_SOLD_OUT;
                }
                break;
        }

        $visualStatus = isset(self::VISUAL_STATUSES[$showStatus]) ?
            self::VISUAL_STATUSES[$showStatus] :
            self::VISUAL_STATUSES[Show::STATUS_DEFAULT];

        return [
            'title' => $show->name(),
            'tickets_left' => ($ticketsLeft - $ticketsSold),
            'tickets_available' => ($ticketsAvailable - $ticketsSold),
            'status' => $visualStatus,
            'price' => $this->ticketPriceService->checkPrice($show, $showDate)
        ];
    }
}
