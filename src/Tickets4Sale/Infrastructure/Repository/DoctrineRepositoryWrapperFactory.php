<?php
namespace Tickets4Sale\Infrastructure\Repository;

use Doctrine\ORM\EntityManagerInterface;

class DoctrineRepositoryWrapperFactory
{
    public static function build(EntityManagerInterface $em, string $className)
    {
        return new DoctrineRepositoryWrapper($em, $className);
    }
}
