<?php
namespace Tickets4Sale\Domain\Show;

use DateTimeInterface;
use LogicException;
use Throwable;

/**
 * Class SalesNotOpenException
 *
 * @package Tickets4Sale\Domain\Show
 */
final class SalesNotOpenException extends LogicException
{
    public function __construct(DateTimeInterface $salesStartDate, $code = 0, Throwable $previous = null)
    {
        $message = "This show's ticket sales only opens on " . $salesStartDate->format('Y-m-d H:i:s');
        parent::__construct($message, $code, $previous);
    }
}
