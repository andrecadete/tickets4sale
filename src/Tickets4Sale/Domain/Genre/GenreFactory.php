<?php
namespace Tickets4Sale\Domain\Genre;

/**
 * Class GenreFactory
 *
 * @package Tickets4Sale\Domain\Genre
 */
class GenreFactory
{
    /**
     * Builds a Genre based on a Genre name, if existent.
     *
     * @param string $name
     * @return Genre
     */
    public static function build(string $name): Genre
    {
        switch (strtolower($name)) {
            case 'drama':
                return new Drama();
                    break;
            case 'comedy':
                return new Comedy();
                    break;
            case 'musical':
                return new Musical();
                    break;
        }
    }
}