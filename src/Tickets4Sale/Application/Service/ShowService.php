<?php
namespace Tickets4Sale\Application\Service;

use InvalidArgumentException;
use Tickets4Sale\Domain\Show\Show;

/**
 * Interface ShowService
 *
 * @package Tickets4Sale\Application\Service
 */
interface ShowService
{
    /**
     * @param Show $show
     * @throws InvalidArgumentException
     */
    public function addShow(Show $show): void;
}
