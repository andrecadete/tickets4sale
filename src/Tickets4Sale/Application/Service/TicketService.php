<?php
namespace Tickets4Sale\Application\Service;

use Tickets4Sale\Application\Ticket\PurchaseTicketRequest;
use Tickets4Sale\Application\Ticket\PurchaseTicketResponse;
use Tickets4Sale\Application\Ticket\TicketPriceRequest;
use Tickets4Sale\Application\Ticket\TicketPriceResponse;
use Tickets4Sale\Domain\Show\SalesClosedException;
use Tickets4Sale\Domain\Show\SalesNotOpenException;
use Tickets4Sale\Domain\Show\ShowDoesNotExistException;
use Tickets4Sale\Domain\Ticket\TicketsSoldOutException;

/**
 * A Service to handle Tickets
 *
 * Interface TicketService
 *
 * @package Tickets4Sale\Application\Service
 */
interface TicketService
{
    /**
     * Purchases a ticket for a given Show date - if available.
     * For now it assumes the ticket has been purchased.
     *
     * @param PurchaseTicketRequest $request
     * @return PurchaseTicketResponse
     * @throws SalesClosedException
     * @throws SalesNotOpenException
     * @throws TicketsSoldOutException
     * @throws ShowDoesNotExistException
     */
    public function purchaseTicket(PurchaseTicketRequest $request): PurchaseTicketResponse;

    /**
     * Checks a Ticket price for a given Show date
     *
     * @param TicketPriceRequest $request
     * @return TicketPriceResponse
     */
    public function checkPrice(TicketPriceRequest $request): TicketPriceResponse;
}