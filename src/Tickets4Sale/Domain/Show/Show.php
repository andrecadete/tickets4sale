<?php

namespace Tickets4Sale\Domain\Show;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use DateTimeImmutable;
use DateInterval;
use Tickets4Sale\Domain\Entity\Entity;
use Tickets4Sale\Domain\Genre\Genre;

/**
 * Show Entity
 * Class Show
 *
 * @package Tickets4Sale\Domain\Show
 */
class Show implements Entity
{

    /**
     * Theaters where ANY show performs, per order of performance
     */
    const THEATERS = [self::BIG_HALL, self::SMALL_HALL];
    const BIG_HALL = 0;
    const SMALL_HALL = 1;

    /**
     * Days in each Theater
     */
    const THEATER_DAYS = [self::BIG_HALL => 60, self::SMALL_HALL => 40];

    /**
     * Seats available in each Theater
     */
    const THEATER_SEATS = [self::BIG_HALL => 200, self::SMALL_HALL => 100];

    /**
     * How many days counting from the END are discounted
     */
    const DISCOUNT_LAST_DAYS = 20;

    /**
     * The fixed discount %
     */
    const DISCOUNT_AMOUNT = .2;

    /**
     * Statuses of the show
     */
    public const STATUS_SALES_NOT_STARTED = 0;
    public const STATUS_SALES_OPEN = 1;
    public const STATUS_SALES_SOLD_OUT = 2;
    public const STATUS_SALES_CLOSED = 3;
    public const STATUS_DEFAULT = self::STATUS_SALES_NOT_STARTED;

    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var DateTimeImmutable $startDate
     */
    private $startDate;

    /**
     * @var DateTimeImmutable $endDate
     */
    private $endDate;

    /**
     * @var Genre $genre
     */
    private $genre;

    /**
     * @var string $name
     */
    private $name;

    /**
     * Show constructor.
     *
     * @param null|UuidInterface $id
     * @param string             $name
     * @param DateTimeImmutable  $startDate
     * @param Genre              $genre
     */
    public function __construct(
        ?UuidInterface $id = null,
        string $name,
        DateTimeImmutable $startDate,
        Genre $genre
    ) {
        $this->id = !is_null($id) ? $id : Uuid::uuid4();
        $this->name = $name;
        $this->startDate = $startDate;
        $this->endDate = $startDate->add(new \DateInterval('P' . self::duration() . 'D'));
        $this->genre = $genre;
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * Returns the start date
     *
     * @return DateTimeImmutable
     */
    public function startsOn(): DateTimeImmutable
    {
        // workaround for doctrine
        if ($this->startDate instanceof DateTimeImmutable) {
            return $this->startDate;
        }
        return DateTimeImmutable::createFromMutable($this->startDate);
    }

    /**
     * Returns the end date
     *
     * @return DateTimeImmutable
     */
    public function endsOn(): DateTimeImmutable
    {
        // workaround for doctrine
        if ($this->endDate instanceof DateTimeImmutable) {
            return $this->endDate;
        }
        return DateTimeImmutable::createFromMutable($this->endDate);
    }

    /**
     * @return Genre
     */
    public function whichGenre(): Genre
    {
        $genreClassName = 'Tickets4Sale\\Domain\\Genre\\' . (string)$this->genre;
        return new $genreClassName;
    }

    /**
     * The date when the Show is open for sale, overall or on specific $day
     *
     * @param  DateTimeImmutable|null $day
     * @return DateTimeImmutable
     */
    public function ticketSalesStartOn(?DateTimeImmutable $day = null): DateTimeImmutable
    {
        $dateToCompare = !is_null($day) ? $day : $this->startsOn();

        return $dateToCompare->sub(new DateInterval('P25D'));
    }

    /**
     * The date when the Show is sold out, overall or on specific $day
     *
     * @param  DateTimeImmutable|null $day
     * @return DateTimeImmutable
     */
    public function ticketSalesEndOn(?DateTimeImmutable $day = null): DateTimeImmutable
    {
        $dateToCompare = !is_null($day) ? $day : $this->endsOn();
        return $dateToCompare->sub(new DateInterval('P5D'));
    }

    /**
     * @return int
     */
    public function duration()
    {
        return array_sum(self::THEATER_DAYS);
    }
}
