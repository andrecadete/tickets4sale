<?php
namespace Tickets4Sale\Tests\Unit\Repository;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Entity\Entity;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapper;
use PHPUnit_Framework_MockObject_MockObject;

/**
 * Class DoctrineAwareRepositoryTest
 *
 * Tests that our Abstract Doctrine Repository implementation is wrapping Doctrine's methods accordingly
 * and it's signature didn't change (return types mainly)
 *
 * @package Tickets4Sale\Tests\Unit\Repository
 */
class DoctrineRepositoryWrapperTest extends TestCase
{
    /**
     * @var EntityManagerInterface|PHPUnit_Framework_MockObject_MockObject $em
     */
    private $externalEntityManager;

    /**
     * @var DoctrineRepositoryWrapper|PHPUnit_Framework_MockObject_MockObject $doctrineRepositoryWrapper
     */
    private $doctrineRepositoryWrapper;

    /**
     * @var ObjectRepository $externalRepository
     */
    private $externalRepository;


    public function setUp()
    {
        $this->externalEntityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()->getMock();

        $this->externalRepository = $this->getMockBuilder(ObjectRepository::class)
            ->disableOriginalConstructor()->getMock();

        $this->externalEntityManager->expects($this->once())->method('getRepository')
            ->willReturn($this->externalRepository);

        $this->doctrineRepositoryWrapper = new DoctrineRepositoryWrapper(
            $this->externalEntityManager,
            'randomClassName'
        );
    }

    /**
     * @test
     */
    public function itWrapsTheFindMethod()
    {
        $someEntity = $this->getMockBuilder(Entity::class)->getMock();
        // pick a random repo extending DoctrineAwareRepository
        $this->externalRepository->expects($this->once())->method('find')->willReturn($someEntity);
        $this->doctrineRepositoryWrapper->find(1);
    }

    /**
     * @test
     */
    public function itWrapsTheFindByMethod()
    {
        // pick a random repo extending DoctrineAwareRepository
        $this->externalRepository->expects($this->once())->method('findBy')->willReturn([]);
        $this->doctrineRepositoryWrapper->findBy([]);
    }

    /**
     * The persist() method is not public. It is made to be used by descendant classes, allowing them to implement their
     * own data persistence strategy while allowing stronger type-hinting.
     * Therefore to test this method we need to call a descendant-class method.
     * The chosen random descendant (DoctrineShowRepository) has a public method save() that in turn calls persist().
     *
     * @test
     */
    public function itWrapsThePersistAndFlushMethods()
    {
        // pick a random repo extending DoctrineAwareRepository
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $this->externalEntityManager->expects($this->once())->method('persist')->with($show);
        $this->externalEntityManager->expects($this->once())->method('flush');

        $this->doctrineRepositoryWrapper->save($show);
    }
}
