<?php

namespace Tickets4Sale\Tests\Unit\Domain\Show;

use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Genre\Comedy;
use Tickets4Sale\Domain\Genre\Genre;
use Tickets4Sale\Domain\Genre\Musical;
use Tickets4Sale\Domain\Show\Show;
use DateTimeInterface;
use DateTimeImmutable;
use DateInterval;

/**
 * Class ShowTest
 * @package Tickets4Sale\Tests\Unit\Domain\Show
 */
class ShowTest extends TestCase
{
    private function getMockGenre()
    {
        return $this->getMockForAbstractClass(
            Genre::class,
            [],
            '',
            false,
            true,
            true,
            ['__toString']
        );
    }
    /**
     * Tests that no ticket can be sold before 25 days prior to the opening of a show
     *
     * @test
     */
    public function showSalesOpenOnlyTwentyFiveDaysBeforeStartDate()
    {
        $mockGenre =  $this->getMockGenre();
        // a show that starts in 25 days, should already allow purchase of tickets
        $openSalesShow = new Show(
            null,
            'someName',
            (new DateTimeImmutable())->add(new DateInterval('P25D')),
            $mockGenre
        );
        $this->assertTrue($openSalesShow->ticketSalesStartOn()->getTimestamp() <= time());

        // a show that starts in 26 days, should throw exception
        $closedSalesShow = new Show(
            null,
            'someName',
            (new DateTimeImmutable())->add(new DateInterval('P26D')),
            $mockGenre
        );
        $this->assertFalse($closedSalesShow->ticketSalesEndOn()->getTimestamp() <= time());
    }

    /**
     * Tests that no ticket can be sold 5 days prior to the close date
     *
     * @test
     */
    public function showSalesEndFiveDaysBeforeStartDate()
    {
        $mockGenre =  $this->getMockGenre();
        // a show that ends in 10 days (started 90 days ago), sales should be open
        $openSalesShow = new Show(
            null,
            'someName',
            (new DateTimeImmutable())->sub(new DateInterval('P90D')),
            $mockGenre
        );
        $this->assertFalse($openSalesShow->ticketSalesEndOn()->getTimestamp() <= time());

        // a show that ends in 4 days (started 96 days ago), sales should be closed
        $closedSalesShow = new Show(
            null,
            'someName',
            (new DateTimeImmutable())->sub(new DateInterval('P96D')),
            $mockGenre
        );
        $this->assertTrue($closedSalesShow->ticketSalesEndOn()->getTimestamp() <= time());
    }

    /**
     * @test
     */
    public function itHasAStartDate()
    {
        $show = new Show(null, 'someName', new DateTimeImmutable(), $this->getMockGenre());
        $this->assertInstanceOf(DateTimeInterface::class, $show->startsOn());
    }

    /**
     * @test
     */
    public function itHasAnEndDate()
    {
        $show = new Show(null, 'someName', new DateTimeImmutable(), $this->getMockGenre());
        $this->assertInstanceOf(DateTimeInterface::class, $show->endsOn());
    }

    /**
     * @test
     */
    public function itHasAGenre()
    {
        $mockGenre = $this->getMockGenre();
        $show = new Show(null, 'someName', new DateTimeImmutable(), $mockGenre);
        $mockGenre->expects($this->once())->method('__toString')->willReturn('Comedy');
        $this->assertInstanceOf(Genre::class, $show->whichGenre());
    }

    /**
     * @test
     */
    public function theEndDateIsAlwaysAHundredDaysAfterTheStartDate()
    {
        $now = new DateTimeImmutable();
        $hundredDaysFromNow = $now->add(new DateInterval('P100D'));
        $show = new Show(null, 'someName', $now, $this->getMockGenre());
        $this->assertEquals($hundredDaysFromNow->format('Y-m-d'), $show->endsOn()->format('Y-m-d'));
    }
}
