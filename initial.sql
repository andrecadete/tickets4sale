CREATE USER phpuser@localhost IDENTIFIED BY "test1234";

CREATE DATABASE IF NOT EXISTS tickets4sale;
GRANT ALL ON tickets4sale.* to phpuser@localhost;

CREATE DATABASE IF NOT EXISTS tickets4sale_test;
GRANT ALL ON tickets4sale_test.* to phpuser@localhost;
