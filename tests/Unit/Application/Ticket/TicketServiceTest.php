<?php
namespace Tickets4Sale\Tests\Unit\Application\Ticket;

use Tickets4Sale\Application\Ticket\PurchaseTicketRequest;
use Tickets4Sale\Application\Ticket\TicketPriceRequest;
use Tickets4Sale\Application\Ticket\TicketService;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowRepository;
use Tickets4Sale\Domain\Show\ShowStatusService;
use Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService;
use Tickets4Sale\Domain\Ticket\TicketPriceService;
use Tickets4Sale\Domain\Ticket\TicketRepository;
use PHPUnit_Framework_MockObject_MockObject;
use Tickets4Sale\Application\Service\TicketService as TicketServiceContract;
use Tickets4Sale\Tests\Fixtures\InMemoryFixtureAwareKernelTestCase;
use DateTimeImmutable;
use DateInterval;
use ArrayIterator;

/**
 * Class TicketServiceTest
 */
class TicketServiceTest extends InMemoryFixtureAwareKernelTestCase
{

    /**
     * @var PHPUnit_Framework_MockObject_MockObject $ticketRepository
     */
    private $ticketRepository;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject $showRepository
     */
    private $showRepository;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject $ticketAvailabilityService
     */
    private $ticketAvailabilityService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject $ticketPriceService
     */
    private $ticketPriceService;

    /**
     * @var TicketServiceContract $ticketService
     */
    private $ticketService;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject $showStatusService
     */
    private $showStatusService;

    /**
     * A Show that starts in 20 days (Sales open)
     * @var Show $show
     */
    private $show;

    public function setUp()
    {
        parent::setUp();

        $this->ticketRepository = $this->getMockBuilder(TicketRepository::class)->getMock();
        $this->ticketAvailabilityService = $this->getMockBuilder(TicketMaximumAvailabilityService::class)->getMock();
        $this->ticketPriceService = $this->getMockBuilder(TicketPriceService::class)->getMock();
        $this->showStatusService = $this->getMockBuilder(ShowStatusService::class)->getMock();
        $this->showRepository = $this->getMockBuilder(ShowRepository::class)->getMock();
        $startDate = (new DateTimeImmutable())->add(new DateInterval('P20D'));
        $this->show = $this->fixtures->create(Show::class, ['startDate' => $startDate])->first();

        $this->ticketService = new TicketService(
            $this->ticketRepository,
            $this->ticketAvailabilityService,
            $this->ticketPriceService,
            $this->showStatusService,
            $this->showRepository
        );
    }

    /**
     * @test
     * @expectedException \Tickets4Sale\Domain\Show\SalesNotOpenException
     */
    public function itDoesNotSellOnNoOpenSales()
    {
        $request = new PurchaseTicketRequest($this->show->name(), $this->show->startsOn());
        $this->showStatusService->expects($this->once())->method('checkstatus')
            ->willReturn(Show::STATUS_SALES_NOT_STARTED);

        // it does NOT persist
        $this->showRepository->expects($this->once())->method('findBy')->willReturn(new ArrayIterator([$this->show]));
        $this->ticketRepository->expects($this->never())->method('save');

        $this->ticketService->purchaseTicket($request);
    }

    /**
     * @test
     * @expectedException \Tickets4Sale\Domain\Show\SalesClosedException
     */
    public function itDoesNotSellOnPastSales()
    {
        $request = new PurchaseTicketRequest($this->show->name(), $this->show->startsOn());
        $this->showStatusService->expects($this->once())->method('checkstatus')
            ->willReturn(Show::STATUS_SALES_CLOSED);

        // it does NOT persist
        $this->showRepository->expects($this->once())->method('findBy')->willReturn(new ArrayIterator([$this->show]));
        $this->ticketRepository->expects($this->never())->method('save');

        $result = $this->ticketService->purchaseTicket($request);
    }

    /**
     * @test
     * @expectedException \Tickets4Sale\Domain\Ticket\TicketsSoldOutException
     */
    public function itDoesNotSellIfTicketsSoldOutDueToLegislation()
    {
        $request = new PurchaseTicketRequest($this->show->name(), $this->show->startsOn());
        $this->ticketRepository->expects($this->never())->method('save');
        $this->showRepository->expects($this->once())->method('findBy')->willReturn(new ArrayIterator([$this->show]));

        // sold out before even checking existing prices
        $this->showStatusService->expects($this->once())->method('checkstatus')
            ->willReturn(Show::STATUS_SALES_SOLD_OUT);
        $this->ticketService->purchaseTicket($request);
    }

    /**
     * @test
     * @expectedException \Tickets4Sale\Domain\Ticket\TicketsSoldOutException
     */
    public function itDoesNotSellIfTicketsSoldOutDueToStock()
    {
        $request = new PurchaseTicketRequest($this->show->name(), $this->show->startsOn());

        $this->showRepository->expects($this->once())->method('findBy')->willReturn(new ArrayIterator([$this->show]));
        $this->showStatusService->expects($this->once())->method('checkstatus')
            ->willReturn(Show::STATUS_SALES_OPEN);
        $this->ticketAvailabilityService->expects($this->once())->method('dailyMaximumAvailable')
            ->willReturn(5);
        $this->ticketRepository->expects($this->once())->method('countSoldTickets')->willReturn(5);

        // it does NOT persist
        $this->ticketRepository->expects($this->never())->method('save');

        $this->ticketService->purchaseTicket($request);
    }

    /**
     * @test
     */
    public function itPersistsAnAvailableAndPurchasedTicket()
    {
        $request = new PurchaseTicketRequest($this->show->name(), $this->show->startsOn());
        $this->showRepository->expects($this->once())->method('findBy')->willReturn(new ArrayIterator([$this->show]));
        $this->showStatusService->expects($this->once())->method('checkstatus')
            ->willReturn(Show::STATUS_SALES_OPEN);
        $this->ticketAvailabilityService->expects($this->once())->method('dailyMaximumAvailable')
            ->willReturn(1);
        $this->ticketRepository->expects($this->once())->method('countSoldTickets')->willReturn(0);

        // it persists!
        $this->ticketRepository->expects($this->once())->method('save');

        $this->ticketService->purchaseTicket($request);
    }

    /**
     * @test
     */
    public function itChecksDomainTicketPrices()
    {
        $request = new TicketPriceRequest($this->show, $this->show->startsOn());
        $this->ticketPriceService->expects($this->once())->method('checkPrice');
        $this->ticketService->checkPrice($request);
    }
}
