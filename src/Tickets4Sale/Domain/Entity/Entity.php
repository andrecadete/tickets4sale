<?php
namespace Tickets4Sale\Domain\Entity;

use Ramsey\Uuid\UuidInterface;

/**
 * Interface Entity
 *
 * @package Tickets4Sale\Domain\Entity
 */
interface Entity
{
    public function id(): UuidInterface;
}
