<?php
namespace Tickets4Sale\Domain\Show;

use LogicException;
use Throwable;

/**
 * Class ShowDoesNotExistException
 * @package Tickets4Sale\Domain\Show
 */
class ShowDoesNotExistException extends LogicException
{
    /**
     * ShowDoesNotExistException constructor.
     *
     * @param string $attemptedShowName
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($attemptedShowName, $code = 0, Throwable $previous = null)
    {
        parent::__construct('The show "' . $attemptedShowName . '" does not exist.', $code, $previous);
    }
}
