<?php
namespace Tickets4Sale\Tests\Fixtures;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use TijmenWierenga\Bogus\Storage\InMemoryStorageAdapter;
use TijmenWierenga\Bogus\Config\ConfigFile;
use TijmenWierenga\Bogus\Config\YamlConfig;
use TijmenWierenga\Bogus\Generator\MappingFile\MappingFileFactory;
use TijmenWierenga\Bogus\Fixtures;

class InMemoryFixtureAwareKernelTestCase extends KernelTestCase
{
    /**
     * @var Fixtures $fixtures
     */
    protected $fixtures;

    public function setUp()
    {
        $kernel = $this->bootKernel();
        $configFile = new ConfigFile($kernel->getRootDir() . '/../tests/fixtures.yaml');
        $config = new YamlConfig($configFile);
        $factory = new MappingFileFactory($config);

        $storageAdapter = new InMemoryStorageAdapter();
        $this->fixtures = new Fixtures($storageAdapter, $factory);
    }
}