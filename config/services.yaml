# Put parameters here that don't need to change on each machine where the app is deployed
# https://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: 'en'

services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
        public: false       # Allows optimizing the container by removing unused services; this also means
                            # fetching services directly from the container via $container->get() won't work.
                            # The best practice is to be explicit about your dependencies anyway.

    # Repository Wrappers
    # These are simple entries in the container using the same wrapper class and a Factory to inject the different args

    tickets4sale.infrastructure.repository.show.wrapper:
        class: Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapper
        factory: ['Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapperFactory', build]
        arguments: ['@doctrine.orm.default_entity_manager','Tickets4Sale\Domain\Show\Show']

    tickets4sale.infrastructure.repository.ticket.wrapper:
            class: Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapper
            factory: ['Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapperFactory', build]
            arguments: ['@doctrine.orm.default_entity_manager','Tickets4Sale\Domain\Ticket\Ticket']

    # Repositories

    tickets4sale.infrastructure.repository.show:
        class: Tickets4Sale\Infrastructure\Show\DoctrineShowRepository
        arguments:
            - '@tickets4sale.infrastructure.repository.show.wrapper'

    tickets4sale.infrastructure.repository.ticket:
            class: Tickets4Sale\Infrastructure\Ticket\DoctrineTicketRepository
            arguments:
                - '@tickets4sale.infrastructure.repository.ticket.wrapper'

    # Repository auto-wire namespaces

    Tickets4Sale\Domain\Show\ShowRepository: "@tickets4sale.infrastructure.repository.show"
    Tickets4Sale\Domain\Ticket\TicketRepository: "@tickets4sale.infrastructure.repository.ticket"

    # Application Services

    tickets4sale.application.service.show-inventory:
        class: Tickets4Sale\Application\Show\ShowInventoryService

    tickets4sale.application.service.show:
        class: Tickets4Sale\Application\Show\ShowService

    tickets4sale.application.service.ticket:
        class: Tickets4Sale\Application\Ticket\TicketService

    # Application Services auto-wire namespaces

    Tickets4Sale\Application\Service\ShowService: "@tickets4sale.application.service.show"
    Tickets4Sale\Application\Service\ShowInventoryService: "@tickets4sale.application.service.show-inventory"
    Tickets4Sale\Application\Service\TicketService: "@tickets4sale.application.service.ticket"

    # Application Commands

    tickets4sale.application.command.custom.show.inventory.status:
        class: Tickets4Sale\Application\Console\CustomShowListTicketStatusCommand
        arguments: ['%kernel.root_dir%', '@tickets4sale.application.service.show-inventory']
        tags:
            - { name: 'console.command' }

    tickets4sale.application.command.imported.show.inventory.status:
        class: Tickets4Sale\Application\Console\ImportedShowListTicketStatusCommand
        arguments: ['@tickets4sale.application.service.show-inventory']
        tags:
            - { name: 'console.command' }

    tickets4sale.application.command.show.import:
        class: Tickets4Sale\Application\Console\ImportShowListCommand
        arguments: ['%kernel.root_dir%', "@tickets4sale.application.service.show"]
        tags:
            - { name: 'console.command' }

    tickets4sale.application.command.ticket.purchase:
        class: Tickets4Sale\Application\Console\PurchaseTicketCommand
        arguments: ["@tickets4sale.application.service.ticket"]
        tags:
            - { name: 'console.command' }

    # Domain Services

    tickets4sale.domain.service.ticket.availability:
        class: Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService

    tickets4sale.domain.service.ticket.price:
        class: Tickets4Sale\Domain\Ticket\TicketPriceService

    tickets4sale.domain.service.show.status:
        class: Tickets4Sale\Domain\Show\ShowStatusService

    # Domain Services auto-wire namespaces
    Tickets4Sale\Domain\Ticket\TicketMaximumAvailabilityService: "@tickets4sale.domain.service.ticket.availability"
    Tickets4Sale\Domain\Ticket\TicketPriceService: "@tickets4sale.domain.service.ticket.price"
    Tickets4Sale\Domain\Show\ShowStatusService: "@tickets4sale.domain.service.show.status"


    # Application Controllers
    # Special section since they don't really belong in the core Tickets4Sale namespace:
    # For the sake of simplicity and development (and since there's a single controller)
    # they are placed within the "package", but they could easily be somewhere else,
    # for example a separate PsrContainerInterface-compatible (or wrapper) front-end application
    # requiring this as a dependency and merging containers or registering these services themselves
    Tickets4Sale\Application\Http\Controller\:
        resource: '../src/Tickets4Sale/Application/Http/Controller/'

