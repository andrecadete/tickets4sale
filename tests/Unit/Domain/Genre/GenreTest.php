<?php
namespace Tickets4Sale\Tests\Unit\Domain\Genre;

use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Genre\Comedy;
use Tickets4Sale\Domain\Genre\Drama;
use Tickets4Sale\Domain\Genre\Genre;
use Tickets4Sale\Domain\Genre\Musical;

class GenreTest extends TestCase
{
    /**
     * @test
     * @dataProvider genres
     */
    public function itUsesLateStaticBindingForStringRepresentation(Genre $genre, $genreName)
    {
        // first ensure this is a Genre
        $this->assertInstanceOf(Genre::class, $genre);
        // now the string representation must match "Musical"
        $this->assertEquals($genreName, (string)$genre);
    }

    /**
     * @return array
     */
    public function genres(): array
    {
        return [
            [new Musical(), 'Musical'],
            [new Comedy(), 'Comedy'],
            [new Drama(), 'Drama'],
        ];
    }
}