<?php
namespace Tickets4Sale\Application\Show;

use Tickets4Sale\Domain\Show\Show;
use DateTimeInterface;
use DateTimeImmutable;

/**
 * Class ShowInventoryStatusResponse
 *
 * @package Tickets4Sale\Application\Show
 */
class ShowInventoryStatusResponse
{
    /**
     * @var array $shows
     */
    private $inventory;

    /**
     * @var DateTimeInterface $queryDate
     */
    private $queryDate;

    /**
     * @var DateTimeInterface $showDate
     */
    private $showDate;

    public function __construct(array $inventory, DateTimeImmutable $queryDate, DateTimeImmutable $showDate)
    {
        $this->inventory = $inventory;
        $this->queryDate = $queryDate;
        $this->showDate = $showDate;
    }

    /**
     * @return Show[]
     */
    public function showInventory(): array
    {
        return $this->inventory;
    }

    /**
     * @return DateTimeImmutable
     */
    public function queriedOn(): DateTimeImmutable
    {
        return $this->queryDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function when(): DateTimeImmutable
    {
        return $this->showDate;
    }
}
