<?php

namespace Tickets4Sale\Domain\Genre;

/**
 * Class Drama
 *
 * @package Tickets4Sale\Domain\Genre
 */
class Drama extends Genre
{
    protected static $price = 40;
}
