<?php
namespace Tickets4Sale\Tests\Features\Context;

use Behat\Behat\Context\Context;
use Symfony\Component\HttpKernel\KernelInterface;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Tickets4Sale\Application\Service\ShowService;
use Tickets4Sale\Application\Service\TicketService;
use Tickets4Sale\Application\Ticket\PurchaseTicketRequest;
use Tickets4Sale\Domain\Genre\GenreFactory;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowRepository;
use DateTimeImmutable;
use DateInterval;
use Tickets4Sale\Domain\Ticket\Ticket;
use Tickets4Sale\Domain\Ticket\TicketRepository;

/**
 * Class ShowListContext
 * @package Tickets4Sale\Tests\Features\Context
 */
class ShowListContext extends TestDatabaseContext implements Context
{

    /**
     * @var ShowService $showService
     */
    private $showService;

    /**
     * @var TicketService $ticketService
     */
    private $ticketService;

    /**
     * @var Show $show
     */
    private $show;

    /**
     * @var Response $response
     */
    private $response;

    /**
     * @var Router $router
     */
    private $router;

    /**
     * @var KernelInterface $kernel
     */
    protected $kernel;

    /**
     * ShowListContext constructor.
     *
     * @param KernelInterface $kernel
     * @param ShowService $showService
     * @param TicketService $ticketService
     */
    public function __construct(
        KernelInterface $kernel,
        ShowService $showService,
        TicketService $ticketService
    ) {
        $this->kernel = $kernel;
        $consoleApplication = new Application($this->kernel);
        $consoleApplication->setAutoExit(false);
        $this->showService = $showService;
        $this->ticketService = $ticketService;
        $this->router = $kernel->getContainer()->get('router');
        parent::__construct($kernel);
    }

    /**
     * @Given there is a Show of the Genre :arg2, starting in :arg3 days
     */
    public function thereIsAShowOfTheGenreStartingInDays($arg2, $arg3)
    {
        $startDate = (new DateTimeImmutable())->add(new DateInterval('P' . $arg3 . 'D'))->setTime(0, 0, 0);
        $this->show = new Show(null, (Factory::create())->name(), $startDate, GenreFactory::build($arg2));
        $this->showService->addShow($this->show);
    }

    /**
     * @When I visit the show dashboard page
     */
    public function iVisitTheShowDashboardPage()
    {
        $route = $this->router->generate('dashboard');
        $this->response = $this->kernel->handle(Request::create($route, 'GET'));
        $this->assertEquals(200, $this->response->getStatusCode());
    }


    /**
     * @When I select the Show date field with a date :arg1 days in the future
     */
    public function iSelectTheShowDateFieldWithADateDaysInTheFuture($arg1)
    {
        $route = $this->router->generate('dashboard');
        $desiredDate = (new DateTimeImmutable())->add(new DateInterval('P' . $arg1 . 'D'))->setTime(0, 0, 0);
        $this->response = $this->kernel->handle(
            Request::create($route, 'GET', ['show_date' => $desiredDate->format('Y-m-d')])
        );
        $this->assertEquals(200, $this->response->getStatusCode());
    }

    /**
     * @Then I will see the status :arg1
     */
    public function iWillSeeTheStatus($arg1)
    {
        $this->assertContains($arg1, $this->response->getContent());
    }

    /**
     * @When :arg1 Tickets were already sold for this Show in :arg2 days
     */
    public function ticketsWereAlreadySoldForThisShowInDays($arg1, $arg2)
    {
        $desiredDate = (new DateTimeImmutable())->add(new DateInterval('P' . $arg2 . 'D'))->setTime(0, 0, 0);
        for ($i = 1; $i <= $arg1; $i++) {
            $ticket = new Ticket(null, $desiredDate, $this->show, 70);
            $purchaseTicketRequest = new PurchaseTicketRequest($this->show->name(), $desiredDate);
            $this->ticketService->purchaseTicket($purchaseTicketRequest);
        }
    }

    /**
     * @BeforeScenario
     */
    public function stuff()
    {
        parent::cleanDB();
    }
}
