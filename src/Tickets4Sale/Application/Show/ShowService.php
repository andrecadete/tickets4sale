<?php
namespace Tickets4Sale\Application\Show;

use Tickets4Sale\Application\Service\ShowService as ShowServiceContract;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowAlreadyExistsException;
use Tickets4Sale\Domain\Show\ShowRepository;
use Exception;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ShowService implements ShowServiceContract
{
    /**
     * @var ShowRepository $showRepository
     */
    private $showRepository;

    /**
     * ShowService constructor.
     *
     * @param ShowRepository $showRepository
     */
    public function __construct(ShowRepository $showRepository)
    {
        $this->showRepository = $showRepository;
    }

    /**
     * Attempts to add a Show to the storage layer
     *
     * @param  Show $show
     * @throws Exception
     */
    public function addShow(Show $show): void
    {
        try {
            $this->showRepository->save($show);
        } catch (UniqueConstraintViolationException $e) {
            throw new ShowAlreadyExistsException(
                $show,
                0,
                $e
            );
        } catch (Exception $e) {
            throw new Exception(
                'Unexpected Infrastructural fail when trying to persist provided Show. ' .
                'Check previous Exception for details.',
                0,
                $e
            );
        }
    }
}