<?php
namespace Tickets4Sale\Application\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Tickets4Sale\Application\Service\ShowInventoryService;
use Tickets4Sale\Application\Show\ShowInventoryAccessRequest;
use Tickets4Sale\Application\Show\ShowInventoryStatusResponse;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use DateTimeImmutable;
use Tickets4Sale\Infrastructure\Importer\ShowCsvImporter;

/**
 * Class ImportedShowListTicketStatusCommand
 *
 * @package Tickets4Sale\Application\Console
 */
class ImportedShowListTicketStatusCommand extends ShowListTicketStatusCommand
{
    /**
     * @var ShowInventoryService
     */
    protected $inventoryService;

    public function __construct(
        ShowInventoryService $inventoryService
    ) {
        $this->inventoryService = $inventoryService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('tickets4sale:imported-inventory:status')
            ->setDescription('Check Inventory Status of a given list of Shows')
            ->setHelp("Receives a Show date to check availability against current system-imported Shows")
            ->addArgument(
                'show-date',
                InputArgument::REQUIRED,
                "The date of the Shows you're checking the status."
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {

        $now = new DateTimeImmutable();
        $inputShowDate  = $input->getArgument('show-date');
        $showDate = DateTimeImmutable::createFromFormat('Y-m-d', $inputShowDate);

        $inventory = $this->inventoryService->readInventory(
            new ShowInventoryAccessRequest($showDate)
        )->access();

        $output->writeln(
            json_encode(
                ['inventory' => $this->inventoryStatus($inventory, $now, $showDate)->showInventory()],
                JSON_PRETTY_PRINT
            )
        );
    }
}
