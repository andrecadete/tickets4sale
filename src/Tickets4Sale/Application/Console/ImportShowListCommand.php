<?php
namespace Tickets4Sale\Application\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use DateTimeImmutable;
use Tickets4Sale\Application\Service\ShowService;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Infrastructure\Importer\Importer;
use Tickets4Sale\Infrastructure\Importer\ShowCsvImporter;

/**
 * Class class ImportShowListCommand extends Command

 * @package Tickets4Sale\Application\Console
 */
class ImportShowListCommand extends Command
{
    /**
     * @var DecoderInterface $csvEncoder
     */
    private $decoder;

    /**
     * @var string $basePath
     */
    private $basePath;

    /**
     * @var ShowService $showRepository
     */
    private $showService;

    public function __construct(string $basePath, ShowService $showService, DecoderInterface $csvDecoder)
    {
        $this->basePath = $basePath;
        $this->decoder = $csvDecoder;
        $this->showService = $showService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('tickets4sale:show:import')
            ->setDescription('Imports a given CSV list of Shows. Can filter through specific date.')
            ->setHelp(
                "Receives a list of Shows in CSV format to import to the system. \n\r
            Format: [title, opening-day (YYYY-MM-DD), genre]"
            )
            ->addArgument(
                'file-path',
                InputArgument::REQUIRED,
                'Path to input file - relative from root project dir.' .
                'Example: file-path="../externalFolder/file.csv" / file-path="tests/data/someFile.csv"'
            )->addArgument(
                'filter-date',
                InputArgument::OPTIONAL,
                "Pass a specific date to import data from. All other records will be ignored"
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        $inputFilePath = $input->getArgument('file-path');
        $inputFilterDate = !is_null($input->getArgument('filter-date')) ?
            DateTimeImmutable::createFromFormat('Y-m-d', $input->getArgument('filter-date')) :
            null
        ;

        // adjust to midnight
        $adjustedFilterDate = $inputFilterDate ? $inputFilterDate->setTime(0, 0, 0) : null;

        $importer = $this->getImporter($inputFilePath, $adjustedFilterDate);

        /**
         * @var Show[] $shows
         */
        $shows = $importer->import();

        $output->writeln(
            [
                "Found " . count($shows) . ' Shows in the input file' .
                (!is_null($adjustedFilterDate) ?
                    ' that run in the chosen date [' . $adjustedFilterDate->format('Y-m-d') . '].'  : '.'
                ),
                "",
                "Proceeding with import now...",
                "",
            ]
        );

        $this->addShows($shows, $output);

    }

    /**
     * @param Show[]          $shows
     * @param OutputInterface $output
     */
    private function addShows(array $shows, OutputInterface $output): void
    {
        $imported = 0;
        $ignored = 0;

        foreach ($shows as $show) {
            try {
                $this->showService->addShow($show);
                $imported++;
            } catch (\Exception $e) {
                $output->writeln(
                    [
                        'Error found when trying to add show "' . $show->name() . '". Details below:',
                        "",
                        $e->getMessage(),
                        "",
                    ]
                );
                $ignored++;
            }
        }

        $output->writeln(
            [
                "----------------------------------",
                "Finished import process!",
                "",
                "Imported: " . $imported,
                "Ignored: " . $ignored
            ]
        );
    }

    /**
     * @param string                 $inputFilePath
     * @param DateTimeImmutable|null $showDate
     * @return Importer
     */
    private function getImporter(string $inputFilePath, ?DateTimeImmutable $showDate = null): Importer
    {
        if (!is_null($showDate)) {
            return new ShowCsvImporter(
                $this->basePath . '/../' . $inputFilePath,
                $this->decoder,
                $showDate
            );
        }

        return new ShowCsvImporter($this->basePath . '/../' . $inputFilePath, $this->decoder);
    }

}
