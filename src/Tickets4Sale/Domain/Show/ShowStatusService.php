<?php
namespace Tickets4Sale\Domain\Show;

use DateTimeImmutable;

/**
 * Validates that Show is allowed to sell Tickets.
 *
 * Class ShowStatusService
 *
 * @package Tickets4Sale\Domain\Show
 */
class ShowStatusService
{
    /**
     * @param Show              $show
     * @param DateTimeImmutable $queryDate
     * @param DateTimeImmutable $desiredShowDate
     *
     * @return int
     */
    public function checkStatus(Show $show, DateTimeImmutable $queryDate, DateTimeImmutable $desiredShowDate): int
    {
        $salesStart = $show->ticketSalesStartOn($desiredShowDate);
        $daySalesEnd = $show->ticketSalesEndOn($desiredShowDate);
        $status = Show::STATUS_SALES_OPEN;

        // if we're requesting a ticket before or after the sales have started / ended
        if ($salesStart->getTimestamp() > $queryDate->getTimestamp()) {
            $status = Show::STATUS_SALES_NOT_STARTED;
        } elseif ($queryDate->getTimestamp() > $daySalesEnd->getTimestamp()) {
            $status =  Show::STATUS_SALES_SOLD_OUT;
        }

        // if we're requesting a ticket for a show day that has finished performing
        if ($desiredShowDate->getTimestamp() > $show->endsOn()->getTimestamp()) {
            $status = Show::STATUS_SALES_CLOSED;
        }

        return $status;
    }
}
