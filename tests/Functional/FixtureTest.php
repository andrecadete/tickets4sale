<?php

namespace Tickets4Sale\Tests\Functional;

use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Tests\Fixtures\InMemoryFixtureAwareKernelTestCase;

/**
 * Class TestTest
 * @package Tickets4Sale\Tests\Unit
 */
class FixtureTest extends InMemoryFixtureAwareKernelTestCase
{
    /**
     * Tests if Fixtures are working overall with a randomly picked Entity
     *
     * @test
     */
    public function fixturesAreLoaded()
    {
        // default ENV is "test"
        $showFixture = $this->fixtures->create(Show::class, [], 1);
        $this->assertInstanceOf(Show::class, $showFixture->first());
    }
}
