<?php
namespace Tickets4Sale\Domain\Ticket;

use LogicException;
use Throwable;

/**
 * Class TicketsSoldOutException
 *
 * @package Tickets4Sale\Domain\Ticket
 */
final class TicketsSoldOutException extends LogicException
{
    public function __construct(string $showName = '', $code = 0, Throwable $previous = null)
    {
        $message = "There are no more tickets available for this Show. " .
            ($showName ? '(' . $showName . ')' : '');
        parent::__construct($message, $code, $previous);
    }
}