<?php
namespace Tickets4Sale\Infrastructure\Importer;

use DateTimeImmutable;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Tickets4Sale\Domain\Genre\GenreFactory;
use Tickets4Sale\Domain\Show\Show;

class ShowCsvImporter implements Importer
{
    /**
     * @var string $inputFilePath
     */
    private $inputFilePath;

    /**
     * @var DateTimeImmutable|null $filterShowDate
     */
    private $filterShowDate;


    /**
     * @var DecoderInterface $csvDecoder
     */
    private $csvDecoder;

    /**
     * @var array $decodedData
     */
    private $decodedData;

    /**
     * ShowCsvImporter constructor.
     *
     * @param string                 $inputFilePath
     * @param DecoderInterface       $csvDecoder
     * @param DateTimeImmutable|null $filterShowDate
     */
    public function __construct(
        string $inputFilePath,
        DecoderInterface $csvDecoder,
        ?DateTimeImmutable $filterShowDate = null
    ) {
        $this->inputFilePath = $inputFilePath;
        $this->filterShowDate = $filterShowDate;
        $this->csvDecoder = $csvDecoder;
        $this->decodedData = [];
    }

    /**
     * @return array
     */
    public function import(): array
    {
        $this->decodeData();
        return $this->readData();
    }

    /**
     * Read and decoded the data in the input file
     */
    private function decodeData(): void
    {
        $rawData = file_get_contents($this->inputFilePath);

        if (strlen($rawData)) {
            // add some headers to the file
            $decodableRawData = '"name","date","genre"' . PHP_EOL .
                preg_replace('~\R\R~u', PHP_EOL, $rawData);

            $this->decodedData = $this->csvDecoder->decode(
                $decodableRawData,
                'csv'
            );
        }
    }

    /**
     * Reads the data into something the application understands.
     * Filters out irrelevant data if a specific show date is provided
     *
     * @return array
     */
    private function readData(): array
    {
        $shows = [];

        foreach ($this->decodedData as $showInfo) {

            // adjust to midnight
            $showDate = DateTimeImmutable::createFromFormat('Y-m-d', $showInfo['date'])->setTime(0, 0, 0);

            $show = new Show(
                null,
                trim($showInfo['name']),
                $showDate,
                GenreFactory::build(strtolower($showInfo['genre']))
            );

            // filter out shows that don't run in requested $this->filterShowDate
            if (is_null($this->filterShowDate) 
                || ($show->startsOn()->getTimestamp() <= $this->filterShowDate->getTimestamp() 
                && $show->endsOn()->getTimestamp() >= $this->filterShowDate->getTimestamp())
            ) {
                $shows[] = $show;
            }
        }

        return $shows;
    }
}