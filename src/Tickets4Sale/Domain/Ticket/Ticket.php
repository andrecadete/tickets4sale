<?php
namespace Tickets4Sale\Domain\Ticket;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Tickets4Sale\Domain\Entity\Entity;
use Tickets4Sale\Domain\Show\Show;
use DateTimeInterface;
use DateTimeImmutable;

/**
 * Class Ticket
 *
 * @package Tickets4Sale\Domain\Ticket
 */
class Ticket implements Entity
{
    /**
     * Ticket Statuses
     * Not so useful for now but in place to allow scaling to multiple statuses in the future, i.e. for reservations.
     */
    public const STATUS_CREATED = 0;

    public const STATUS_PURCHASED = 1;

    /**
     * @var UuidInterface $id
     */
    private $id;

    /**
     * @var DateTimeImmutable $showDate
     */
    private $showDate;

    /**
     * @var Show $show
     */
    private $show;

    /**
     * @var DateTimeImmutable $purchasedOn
     */
    private $purchaseDate;

    /**
     * @var float $price
     */
    private $price;

    /**
     * @var int $status
     */
    private $status;

    /**
     * Ticket constructor.
     *
     * @param null|UuidInterface $id
     * @param DateTimeImmutable  $showDate
     * @param Show               $show
     * @param float              $price
     */
    public function __construct(
        ?UuidInterface $id = null,
        DateTimeImmutable $showDate,
        Show $show,
        float $price
    ) {
        $this->id = !is_null($id) ? $id : Uuid::uuid4();
        $this->showDate = $showDate;
        $this->show = $show;
        $this->price = $price;
        $this->status = self::STATUS_CREATED;
        $this->purchaseDate = new DateTimeImmutable();
    }

    /**
     * @return UuidInterface
     */
    public function id(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return DateTimeInterface
     */
    public function when(): DateTimeInterface
    {
        return $this->showDate;
    }

    /**
     * @return Show
     */
    public function whichShow(): Show
    {
        return $this->show;
    }

    /**
     * Checks the Ticket purchase date
     *
     * @return DateTimeInterface
     */
    public function purchasedOn():? DateTimeInterface
    {
        return $this->purchaseDate;
    }

    /**
     * Checks weather or not this Ticket has been purchased
     *
     * @return bool
     */
    public function purchased(): bool
    {
        return $this->status === self::STATUS_PURCHASED;
    }

    /**
     * @return float
     */
    public function cost(): float
    {
        return $this->price;
    }

    public function purchase(): void
    {
        $this->status = self::STATUS_PURCHASED;
        $this->purchaseDate = new DateTimeImmutable();
    }
}
