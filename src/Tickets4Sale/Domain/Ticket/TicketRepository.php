<?php
namespace Tickets4Sale\Domain\Ticket;

use ArrayIterator;
use DateTimeImmutable;
use Tickets4Sale\Domain\Show\Show;

/**
 * Interface ShowRepository
 * Show-specific Repository method signatures. Better type-hint - therefore validation - than if in parent Interface.
 *
 * @package Tickets4Sale\Infrastructure\Show
 */
interface TicketRepository
{
    /**
     * Saves a Show Entity
     *
     * @param Ticket $show
     */
    public function save(Ticket $show): void;

    /**
     * @param string $id
     * @return null|Ticket
     */
    public function find(string $id):? Ticket;

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     * @return null|ArrayIterator
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ):? ArrayIterator;

    /**
     * Counts Tickets sold for a given Show and Show date
     * @param Show $show
     * @param DateTimeImmutable $showDate
     * @return int
     */
    public function countSoldTickets(Show $show, DateTimeImmutable $showDate): int;
}
