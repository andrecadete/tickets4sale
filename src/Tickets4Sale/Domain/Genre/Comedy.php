<?php

namespace Tickets4Sale\Domain\Genre;

/**
 * Class Comedy
 *
 * @package Tickets4Sale\Domain\Genre
 */
class Comedy extends Genre
{
    protected static $price = 50;
}
