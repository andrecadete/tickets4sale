# Note: This is a Functional web test, but not a complete UI test as it only tests client-side Output and not input
# To test both, add Selenium & the behat Mink SeleniumDriver
Feature: The Show inventory status is checked correctly against a live inventory
  As Tickets4Sale
  I want to be able check the live inventory status on web page,
  So that I can quickly give feedback to my customers before they purchase Ticket


  Scenario Outline: Sales status are correctly fetched
    Given there is a Show of the Genre "Musical", starting in 25 days
    When I visit the show dashboard page
    And <sold-tickets> Tickets were already sold for this Show in <day-distance> days
    And I select the Show date field with a date <day-distance> days in the future
    Then I will see the status <status>

    Examples:
      | day-distance | status              | sold-tickets   |
      | 25           | "Open for sale"     | 0              |
      | 25           | "Open for sale"     | 4              |
      | 25           | "Sold out"          | 10             |
      | 26           | "Sales not started" | 0              |
      | 125          | "Sales not started" | 0              |
      | 126          | ""                  | 0              |


