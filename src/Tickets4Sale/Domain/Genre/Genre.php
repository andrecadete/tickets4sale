<?php

namespace Tickets4Sale\Domain\Genre;

use Tickets4Sale\Domain\ValueObject\ValueObject;

/**
 * Class Genre
 *
 * @package Tickets4Sale\Domain\Genre
 */
abstract class Genre implements ValueObject
{
    /**
     * @var int $price
     */
    protected static $price;

    /**
     * @return string
     */
    public function __toString(): string
    {
        // return unqualified class name (fastest method apparently). Should be late static bound.
        return substr(static::class, strrpos(static::class, '\\')+1);
    }

    public function costs(): int
    {
        return static::$price;
    }
}