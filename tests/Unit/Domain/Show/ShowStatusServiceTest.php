<?php

namespace Tickets4Sale\Tests\Unit\Domain\Show;

use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowStatusService;
use DateTimeImmutable;
use DateInterval;

/**
 * Class ShowStatusServiceTest
 * @package Tickets4Sale\Tests\Unit\Domain\Show
 */
class ShowStatusServiceTest extends TestCase
{
    /**
     * Happy flow test, ticket sales are open at the moment we try to buy a Ticket (now)
     * @test
     */
    public function itSucceedsIfSalesAreOpen()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();

        $show->expects($this->once())->method('ticketSalesStartOn')
            ->willReturn((new DateTimeImmutable())->sub(new DateInterval('P1D')));
        $show->expects($this->once())->method('ticketSalesEndOn')
            ->willReturn((new DateTimeImmutable())->add(new DateInterval('P1D')));

        $show->expects($this->once())->method('endsOn')
            ->willReturn((new DateTimeImmutable())->add(new DateInterval('P1D')));

        $this->assertEquals(
            Show::STATUS_SALES_OPEN,
            (new ShowStatusService)->checkStatus($show, $this->buildBogusDate(), $this->buildBogusDate())
        );
    }

    /**
     * Sales didn't open YET
     * @test
     */
    public function itFailsIfSalesAreNotOpen()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();

        $show->expects($this->once())->method('ticketSalesStartOn')
            ->willReturn((new DateTimeImmutable())->add(new DateInterval('P1D')));

        $show->expects($this->once())->method('endsOn')
            ->willReturn((new DateTimeImmutable())->add(new DateInterval('P1D')));

        $this->assertEquals(
            Show::STATUS_SALES_NOT_STARTED,
            (new ShowStatusService)->checkStatus($show, $this->buildBogusDate(), $this->buildBogusDate())
        );
    }

    /**
     * Sales were eventually open, but already closed
     * @test
     */
    public function itFailsIfSalesAlreadyClosed()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();

        $show->expects($this->once())->method('ticketSalesStartOn')
            ->willReturn((new DateTimeImmutable())->sub(new DateInterval('P90D')));

        $show->expects($this->once())->method('ticketSalesEndOn')
            ->willReturn((new DateTimeImmutable())->sub(new DateInterval('P1D')));

        $this->assertEquals(
            Show::STATUS_SALES_CLOSED,
            (new ShowStatusService)->checkStatus($show, $this->buildBogusDate(), $this->buildBogusDate())
        );
    }

    /**
     * @return DateTimeImmutable
     */
    private function buildBogusDate()
    {
        return new DateTimeImmutable();
    }
}
