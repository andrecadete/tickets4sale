<?php
namespace Tickets4Sale\Application\Show;

use Tickets4Sale\Domain\Show\Show;
use DateTimeInterface;
use DateTimeImmutable;

/**
 * Class ShowInventoryStatusRequest
 *
 * @package Tickets4Sale\Application\Show
 */
class ShowInventoryStatusRequest
{
    /**
     * Accepts multiple Shows to allow batch actions
     *
     * @var Show[] $shows
     */
    private $shows;

    /**
     * @var DateTimeInterface $queryDate
     */
    private $queryDate;

    /**
     * @var DateTimeInterface $showDate
     */
    private $showDate;

    public function __construct(array $shows, DateTimeImmutable $queryDate, DateTimeImmutable $showDate)
    {
        $this->shows = $shows;
        $this->queryDate = $queryDate;
        $this->showDate = $showDate->setTime(0, 0, 0);
    }

    /**
     * @return Show[]
     */
    public function whichShows(): array
    {
        return $this->shows;
    }

    /**
     * @return DateTimeImmutable
     */
    public function queriedOn(): DateTimeImmutable
    {
        return $this->queryDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function when(): DateTimeImmutable
    {
        return $this->showDate;
    }
}
