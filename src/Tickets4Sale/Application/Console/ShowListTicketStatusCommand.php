<?php
namespace Tickets4Sale\Application\Console;

use Symfony\Component\Console\Command\Command;
use Tickets4Sale\Application\Service\ShowInventoryService;
use Tickets4Sale\Application\Show\ShowInventoryStatusResponse;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use DateTimeImmutable;

/**
 * Base Class ShowListTicketStatusCommand to be extended by any Command needing Show Inventory Status info
 *
 * @package Tickets4Sale\Application\Console
 */
abstract class ShowListTicketStatusCommand extends Command
{
    /**
     * @var ShowInventoryService
     */
    protected $inventoryService;

    /**
     * @param Show[]            $shows
     * @param DateTimeImmutable $queryDate
     * @param DateTimeImmutable $showDate
     * @return ShowInventoryStatusResponse
     */
    protected function inventoryStatus(
        array $shows,
        DateTimeImmutable $queryDate,
        DateTimeImmutable $showDate
    ): ShowInventoryStatusResponse {
        $request = new ShowInventoryStatusRequest($shows, $queryDate, $showDate);
        return $this->inventoryService->checkInventoryStatus($request);
    }
}
