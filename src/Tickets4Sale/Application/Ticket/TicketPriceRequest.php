<?php
namespace Tickets4Sale\Application\Ticket;

use DateTimeImmutable;
use Tickets4Sale\Domain\Show\Show;

/**
 * Class TicketPriceRequest
 *
 * @package Tickets4Sale\Application\Ticket
 */
class TicketPriceRequest
{
    /**
     * @var $show
     */
    private $show;

    /**
     * @var DateTimeImmutable
     */
    private $showDate;

    /**
     * TicketPriceRequest constructor.
     *
     * @param Show $show
     * @param DateTimeImmutable $showDate
     */
    public function __construct(Show $show, DateTimeImmutable $showDate)
    {
        $this->showDate = $showDate->setTime(0, 0, 0);
        $this->show = $show;
    }

    public function when(): DateTimeImmutable
    {
        return $this->showDate;
    }

    public function whichShow(): Show
    {
        return $this->show;
    }
}
