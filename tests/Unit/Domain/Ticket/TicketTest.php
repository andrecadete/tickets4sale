<?php

namespace Tickets4Sale\Tests\Unit\Domain\Ticket;

use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Show\Show;
use DateTimeInterface;
use DateTimeImmutable;
use Tickets4Sale\Domain\Ticket\Ticket;

/**
 * Class TicketTest
 * @package Tickets4Sale\Tests\Unit\Domain\Ticket
 */
class TicketTest extends TestCase
{

    /**
     * @test
     */
    public function itHasAdate()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $ticket = new Ticket(null, new DateTimeImmutable(), $show, 100);
        $this->assertTrue($ticket->when() instanceof DateTimeInterface);
    }

    /**
     * @test
     */
    public function itHasAShow()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $ticket = new Ticket(null, new DateTimeImmutable(), $show, 100);
        $this->assertTrue($ticket->whichShow() instanceof Show);
    }

    /**
     * @test
     */
    public function itSavesThePurchaseDate()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $ticket = new Ticket(null, new DateTimeImmutable(), $show, 100);
        $ticket->purchase();
        $this->assertEquals(
            (new DateTimeImmutable())->format('Y-m-d'),
            $ticket->purchasedOn()->format('Y-m-d')
        );
    }

    /**
     * @test
     */
    public function itIsPurchasable()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $ticket = new Ticket(null, new DateTimeImmutable(), $show, 100);

        // issued
        $this->assertFalse($ticket->purchased());

        // purchase
        $ticket->purchase();

        // purchased
        $this->assertTrue($ticket->purchased());
    }


}
