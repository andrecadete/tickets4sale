<?php
namespace Tickets4Sale\Tests\Features\Context;

use Behat\Behat\Context\Context;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\KernelInterface;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Ticket\Ticket;

class TestDatabaseContext extends TestCase implements Context
{

    /**
     * @var KernelInterface $kernel
     */
    protected $kernel;

    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * TestDatabaseContext constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->entityManager = $this->kernel->getContainer()->get('doctrine.orm.test_entity_manager');
    }

    public function cleanDB()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->delete(Ticket::class, 'a')->getQuery()->execute();
        $queryBuilder->delete(Show::class, 'a')->getQuery()->execute();
    }
}
