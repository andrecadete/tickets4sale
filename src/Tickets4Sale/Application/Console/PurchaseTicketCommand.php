<?php
namespace Tickets4Sale\Application\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Tickets4Sale\Application\Ticket\PurchaseTicketRequest;
use Tickets4Sale\Application\Service\TicketService;
use DateTimeImmutable;

/**
 * Issues and stores a purchased Ticket, assuming it has already been paid for now.
 *
 * Class PurchaseTicketCommand
 * @package Tickets4Sale\Application\Console
 */
class PurchaseTicketCommand extends Command
{

    /**
     * @var TicketService $ticketService
     */
    private $ticketService;

    /**
     * PurchaseTicketCommand constructor.
     * @param TicketService $ticketService
     */
    public function __construct(TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('tickets4sale:ticket:purchase')
            ->setDescription('Purchases a Ticket for ')
            ->setHelp("Purchases a Ticket by Show name and date")
            ->addArgument(
                'show-name',
                InputArgument::REQUIRED,
                'The Show Name. Must be Quoted if it contains spaces. eg: "COMEDY OF ERRORS"'
            )->addArgument(
                'show-date',
                InputArgument::REQUIRED,
                "The Show date"
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $inputShowDate  = $input->getArgument('show-date');
        $showDate = DateTimeImmutable::createFromFormat('Y-m-d', $inputShowDate);
        $inputShowName = $input->getArgument('show-name');

        $request = new PurchaseTicketRequest($inputShowName, $showDate);

        // we could try catch but for a CLI tool the exceptions thrown by the Service are already pretty descriptive
        // for a web-page though, we'd wanna wrap this call in a nice try-catch to avoid 500s :)
        $ticket = $this->ticketService->purchaseTicket($request)->readTicket();
        $output->writeln(
            [
                'Ticket purchased and issued! Thanks!',
                'Show name:',
                $ticket->whichShow()->name(),
                'Show date:',
                $ticket->when()->format('Y-m-d'),
                'Enjoy!'
            ]
        );
    }
}
