<?php
namespace Tickets4Sale\Domain\Ticket;

use Tickets4Sale\Domain\Show\Show;
use DateTimeImmutable;
use DateInterval;

/**
 * Ticket price checker. The price is not a static value, it requires some Domain logic to decide.
 *
 * @package Tickets4Sale\Domain\Ticket
 */
class TicketPriceService
{

    /**
     * Checks the price of a Ticket according to Domain rules, based on a desired Show date
     *
     * @param Show $show
     * @param DateTimeImmutable $desiredShowDate
     * @return float
     */
    public function checkPrice(Show $show, DateTimeImmutable $desiredShowDate): float
    {
        // get the Genre price
        $rawPrice = $show->whichGenre()->costs();
        // apply discount ,if any

        return $this->applyDiscount($show, $desiredShowDate, $rawPrice);
    }

    /**
     * Applies the currently fixed Show discount to a ticket price, if the picked Show date:
     * - Ends in Show::DISCOUNT_LAST_DAYS
     *
     * @param Show $show
     * @param DateTimeImmutable $desiredShowDate
     * @param float $rawPrice
     *
     * @return float
     */
    private function applyDiscount(Show $show, DateTimeImmutable $desiredShowDate, float $rawPrice): float
    {
        if ($desiredShowDate->getTimestamp() >=
            $show->endsOn()->sub(new DateInterval('P' . Show::DISCOUNT_LAST_DAYS . 'D'))->getTimestamp()
        ) {
            return $rawPrice - ($rawPrice * Show::DISCOUNT_AMOUNT);
        }

        return $rawPrice;
    }
}
