Feature: The Show inventory status is checked correctly against a given list
  As Tickets4Sale
  I want to know the ticket status of a performance for a show,
  So that I can service my customers well

  Background:
    Given the custom-list ticket inventory tool
    And a CSV File "tests/data/story1.csv"

  Scenario Outline: Sales status and ticket availability is correct
    When the custom-list ticket inventory tool is executed with the parameters "<query-date>" and "<show-date>"
    Then there are <tickets-remaining> Tickets remaining, <tickets-available> Tickets available and the status is "<status>" for the Show "<show>"

    Examples:
      | query-date | show-date  | tickets-remaining | tickets-available | show               | status              |
      | 2017-01-01 | 2017-07-01 | 200               | 0                 | "cats"             | "Sales not started" |
      | 2017-01-01 | 2017-07-01 | 200               | 0                 | "comedy of errors" | "Sales not started" |
      | 2017-08-01 | 2017-08-15 | 50                | 5                 | "cats"             | "Open for sale"     |
      | 2017-08-01 | 2017-08-15 | 100               | 10                | "comedy of errors" | "Open for sale"     |
      | 2017-08-01 | 2017-08-15 | 100               | 10                | "everyman"         | "Open for sale"     |