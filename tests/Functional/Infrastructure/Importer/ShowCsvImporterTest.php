<?php
namespace Tickets4Sale\Tests\Functional\Infrastructure\Importer;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Infrastructure\Importer\Importer;
use Tickets4Sale\Infrastructure\Importer\ShowCsvImporter;

/**
 * Full functional ShowCsvImporter test that proves:
 * - It can decode the business' choice of format/structure
 * - It can import the whole provided data set into memory (batch not implemented yet, important test to run!!)
 *
 * Class CsvImporterTest
 * @package Tickets4Sale\Tests\Functional\Infrastructure\Importer
 */
class ShowCsvImporterTest extends KernelTestCase
{

    /**
     * @var string $tempFileName
     */
    private $tempFileName;

    /**
     * @var Importer $importer
     */
    private $importer;

    public function setUp()
    {
        $this->tempFileName = '/tmp/importer_test' . time();
        $decoder = new CsvEncoder();
        $this->importer = new ShowCsvImporter($this->tempFileName, $decoder);
    }

    /**
     * Tests The Ticket4Sales header-less csv format can be imported ( "show-name","show-date","show-genre" )
     * @test
     */
    public function itDecodesTheTicket4SalesCsvFormatIntoDomainObjects()
    {

        file_put_contents(
            $this->tempFileName,
            '"Test Name", "2018-06-06", "Comedy"' . "\n\r" // works with
            . 'Test Name 2,2018-07-06,Drama'                    // and without Quoted values
        );


        /**
         * @var Show[] $data
         */
        $data = $this->importer->import();

        // test quoted values
        $this->assertEquals('Test Name', $data[0]->name());
        $this->assertEquals('2018-06-06', $data[0]->startsOn()->format('Y-m-d'));
        $this->assertEquals('Comedy', $data[0]->whichGenre());

        // test unquoted values
        $this->assertEquals('Test Name 2', $data[1]->name());
        $this->assertEquals('2018-07-06', $data[1]->startsOn()->format('Y-m-d'));
        $this->assertEquals('Drama', $data[1]->whichGenre());
    }


    /**
     * Tests a that a 10.000 dataset can be put into memory without consuming too much memory or time:
     * - Under 50MB memory used
     * - Under 2sec import time
     *
     * @test
     */
    public function itPutsTheWholeDatasetIntoMemory()
    {
        $initialUsage = memory_get_usage(true);
        $startTime = time();

        $oneRow = '"Test Name", "2018-06-06", "Comedy"' . "\n\r";
        file_put_contents(
            $this->tempFileName,
            str_repeat($oneRow, 10000)
        );

        $data = $this->importer->import();

        $this->assertCount(10000, $data);

        $finalTime = time() - $startTime;
        $finalUsage = memory_get_usage(true) - $initialUsage;

        $this->assertLessThan(2, $finalTime);
        $this->assertLessThan(50000000, $finalUsage);

    }
}