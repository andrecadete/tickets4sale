<?php
namespace Tickets4Sale\Infrastructure\Importer;

interface Importer
{
    /**
     * @return array
     */
    public function import(): array;
}
