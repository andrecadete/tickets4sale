<?php
namespace Tickets4Sale\Domain\Ticket;

use Tickets4Sale\Domain\Show\Show;
use DateTimeImmutable;
use DateInterval;
use DateTime;

/**
 * Class TicketAvailabilityService
 * The maximum available tickets in a day depends on Domain concerns,
 * particularly which Theater the show will perform in, at a given Show date.
 *
 * @package Tickets4Sale\Domain\Ticket
 */
class TicketMaximumAvailabilityService
{
    /**
     * Maximum sold amount per day
     */
    const DAILY_AMOUNTS = [Show::BIG_HALL => 10, Show::SMALL_HALL => 5];

    /**
     * Returns the maximum possible amount of Tickets sold in any given Show date
     *
     * @param  Show              $show
     * @param  DateTimeImmutable $intendedShowDate
     * @return mixed
     */
    public function dailyMaximumAvailable(Show $show, DateTimeImmutable $intendedShowDate)
    {
        return self::DAILY_AMOUNTS[$this->whichTheater($show, $intendedShowDate)];
    }

    /**
     * Returns the maximum possible unsold Tickets for a Show, from a given point in time until the Show date
     *
     * @param  Show              $show
     * @param  DateTimeImmutable $queryDate
     * @param  DateTimeImmutable $intendedShowDate
     * @return mixed
     */
    public function totalMaximumAvailable(Show $show, DateTimeImmutable $queryDate, DateTimeImmutable $intendedShowDate)
    {
        $totalTickets = $this->totalShowTickets($show, $intendedShowDate);
        return $totalTickets - $this->calculateOverallSoldTicketsForShowDay($show, $queryDate, $intendedShowDate);
    }

    /**
     * Calculates how many tickets are mandatory to be marked as sold at a given date for the show
     *
     * @param Show              $show
     * @param DateTimeImmutable $queryDate
     * @param DateTimeImmutable $intendedShowDate
     *
     * @return int
     */
    public function calculateOverallSoldTicketsForShowDay(
        Show $show,
        DateTimeImmutable $queryDate,
        DateTimeImmutable $intendedShowDate
    ): int {

        $soldTickets = 0;

        // if the query date is in the "future" against the intended Show date's last sales day
        // or the intended show date, it's always sold out
        if ($queryDate->getTimestamp() > $intendedShowDate->getTimestamp()
            || $queryDate->getTimestamp() > $show->ticketSalesEndOn($intendedShowDate)->getTimestamp()
        ) {
            $soldTickets = $this->totalShowTickets($show, $intendedShowDate);
        } else {
            if ($intendedShowDate->getTimestamp() >= $show->startsOn()->getTimestamp()) {
                $maximumQueryDate =
                    $queryDate->getTimestamp() >= $show->ticketSalesEndOn($intendedShowDate)->getTimestamp() ?
                        $show->ticketSalesEndOn($intendedShowDate) : $queryDate;

                $daysSinceSalesStart =
                    $maximumQueryDate->getTimestamp() > $show->ticketSalesStartOn($intendedShowDate)->getTimestamp() ?
                        $show->ticketSalesStartOn($intendedShowDate)->diff($maximumQueryDate)->format("%a") : 0;

                for ($i = 1; $i < $daysSinceSalesStart; $i++) {
                    $soldTickets += $this->dailyMaximumAvailable(
                        $show,
                        $intendedShowDate
                    );
                }
            }
        }



        return $soldTickets;
    }

    /**
     * @param Show              $show
     * @param DateTimeImmutable $intendedShowDate
     *
     * @return int
     */
    private function totalShowTickets(Show $show, DateTimeImmutable $intendedShowDate): int
    {
        return Show::THEATER_SEATS[$this->whichTheater($show, $intendedShowDate)];
    }

    /**
     * Only 2 Theaters available, simplified logic for now. If this number increases,
     * simply iterate over Show::THEATERS together with Show::THEATER_DAYS to run the same Date add() in order of
     * performance.
     *
     * @param  Show              $show
     * @param  DateTimeImmutable $intendedShowDate
     * @return int
     */
    private function whichTheater(Show $show, DateTimeImmutable $intendedShowDate): int
    {
        $lastBigHallPerformance = $show->startsOn()->add(
            new DateInterval('P' . Show::THEATER_DAYS[Show::BIG_HALL] . 'D')
        );

        if ($intendedShowDate->getTimestamp() > $lastBigHallPerformance->getTimestamp()) {
            return Show::SMALL_HALL;
        }

        return Show::BIG_HALL;
    }
}
