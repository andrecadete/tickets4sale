<?php
namespace Tickets4Sale\Application\Ticket;

use DateTimeImmutable;

/**
 * Class PurchaseTicketRequest
 *
 * @package Tickets4Sale\Application\Ticket
 */
class PurchaseTicketRequest
{
    /**
     * @var DateTimeImmutable $showDate
     */
    private $showDate;

    /**
     * @var string $show
     */
    private $showName;

    /**
     * PurchaseTicketRequest constructor.
     *
     * @param string $showName
     * @param DateTimeImmutable $showDate
     */
    public function __construct(string $showName, DateTimeImmutable $showDate)
    {
        $this->showName = $showName;
        $this->showDate = $showDate->setTime(0, 0, 0);
    }

    /**
     * @return string
     */
    public function whichShow(): string
    {
        return $this->showName;
    }

    /**
     * @return DateTimeImmutable
     */
    public function when(): DateTimeImmutable
    {
        return $this->showDate;
    }
}
