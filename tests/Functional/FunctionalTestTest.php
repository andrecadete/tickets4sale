<?php

namespace Tickets4Sale\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tickets4Sale\Infrastructure\Show\DoctrineShowRepository;

/**
 * Class TestTest
 * @package Tickets4Sale\Tests\Unit
 */
class FunctionalTestTest extends KernelTestCase
{
    /**
     * This test checks if services are available in test ENV.
     *
     * @test
     */
    public function servicesArePublicInKernelTestMode()
    {
        // default ENV is "test"
        $kernel = $this->bootKernel(['environment' => 'test']);
        // get some service, should work
        $showRepository = $kernel->getContainer()->get('tickets4sale.infrastructure.repository.show');
        $this->assertInstanceOf(DoctrineShowRepository::class, $showRepository);
    }

    /**
     * This test checks if services are UNavailable in test ENV.
     *
     * @test
     * @expectedException \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function servicesArePrivateIfNotInKernelTestMode()
    {
        $kernel = $this->bootKernel(['environment' => 'prod']);

        // get some service, should NOT work
        $showRepository = $kernel->getContainer()->get('tickets4sale.infrastructure.repository.show');
        $this->assertInstanceOf(DoctrineShowRepository::class, $showRepository);
    }

}
