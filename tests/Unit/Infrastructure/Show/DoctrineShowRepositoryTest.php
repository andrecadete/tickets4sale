<?php
namespace Tickets4Sale\Tests\Unit\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Infrastructure\Repository\DoctrineRepositoryWrapper;
use Tickets4Sale\Infrastructure\Repository\RepositoryWrapper;
use Tickets4Sale\Infrastructure\Show\DoctrineShowRepository;

/**
 * Class DoctrineAwareShowRepositoryTest
 * Tests that the Wrapper Repo class methods are called rather than the actual DBAL - special importance
 * while using external dependency for DBAL - currently Doctrine.
 *
 * If either the signatures change on the specific &/or wrapper Repositories, or the wrapper methods stop being called
 * all the tests below will intentionally fail. Removing the willReturn() assertions breaks this premise.
 *
 * @package Tickets4Sale\Tests\Unit\Repository
 */
class DoctrineShowRepositoryTest extends TestCase
{

    /**
     * @var RepositoryWrapper|\PHPUnit_Framework_MockObject_MockObject $repositoryWrapper
     */
    private $repositoryWrapper;

    /**
     * @var DoctrineShowRepository $doctrineAwareShowRepository
     */
    private $doctrineShowRepository;


    public function setUp()
    {
        $this->repositoryWrapper = $this->getMockBuilder(RepositoryWrapper::class)
            ->disableOriginalConstructor()->getMock();

        $this->doctrineShowRepository = new DoctrineShowRepository($this->repositoryWrapper);
    }

    /**
     * @test
     */
    public function itFindsThroughWrapper()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $this->repositoryWrapper->expects($this->once())->method('find')->willReturn($show);

        $this->doctrineShowRepository->find(1);
    }

    /**
     * @test
     */
    public function itFindsByThroughWrapper()
    {
        $this->repositoryWrapper->expects($this->once())->method('findBy')
            ->willReturn(new ArrayCollection());
        $this->doctrineShowRepository->findBy([]);
    }

    /**
     * @test
     */
    public function itSavesThroughWrapper()
    {
        $show = $this->getMockBuilder(Show::class)->disableOriginalConstructor()->getMock();
        $this->repositoryWrapper->expects($this->once())->method('save')
            ->willReturn(new ArrayCollection([$show]));
        $this->doctrineShowRepository->save($show);
    }
}
