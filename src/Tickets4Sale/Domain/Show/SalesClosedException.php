<?php
namespace Tickets4Sale\Domain\Show;

use LogicException;
use DateTimeInterface;
use Throwable;

/**
 * Class SalesClosedException
 *
 * @package Tickets4Sale\Domain\Show
 */
final class SalesClosedException extends LogicException
{
    public function __construct(DateTimeInterface $salesEndDate, $code = 0, Throwable $previous = null)
    {
        $message = "This show's ticket sales have closed on " . $salesEndDate->format('Y-m-d H:i:s');
        parent::__construct($message, $code, $previous);
    }
}
