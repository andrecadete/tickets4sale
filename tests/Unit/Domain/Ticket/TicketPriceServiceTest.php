<?php
namespace Tickets4Sale\Tests\Unit\Domain\Ticket;

use Tickets4Sale\Domain\Genre\Comedy;
use Tickets4Sale\Domain\Genre\Genre;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Ticket\TicketPriceService;
use Tickets4Sale\Tests\Fixtures\InMemoryFixtureAwareKernelTestCase;
use DateTimeImmutable;
use DateInterval;

/**
 * Class TicketPriceService
 * @package Tickets4Sale\Tests\Unit\Domain\Ticket
 */
class TicketPriceServiceTest extends InMemoryFixtureAwareKernelTestCase
{

    /**
     * @param Genre $genre
     * @return Show
     */
    private function getShowStartingToday(Genre $genre): Show
    {
        return $this->fixtures->create(
            Show::class,
            ['startDate' => new DateTimeImmutable(), 'genre' => $genre]
        )->first();
    }

    /**
     * @test
     */
    public function itAppliesDiscountOnLastNDays()
    {
        $chosenGenre = new Comedy();
        $show = $this->getShowStartingToday($chosenGenre);

        // get price for chosen Genre
        $genrePrice = $chosenGenre->costs();

        // check current discounted price according to the Show Domain objec
        $expectedDiscountPrice = $genrePrice - ($genrePrice * Show::DISCOUNT_AMOUNT);

        // get the first discount date
        $firstDiscountedDate = $show->endsOn()->sub(new DateInterval('P' . Show::DISCOUNT_LAST_DAYS . 'D'));

        $result = (new TicketPriceService())->checkPrice($show, $firstDiscountedDate);

        $this->assertEquals($expectedDiscountPrice, $result);
    }

    /**
     * @test
     */
    public function itDoesntApplyDiscountBeforeLastNDays()
    {
        $chosenGenre = new Comedy();
        $show = $this->getShowStartingToday($chosenGenre);

        // get price for chosen Genre
        $genrePrice = $chosenGenre->costs();

        // get the first discount date
        $nonDiscountedDate = $show->endsOn()->sub(
            new DateInterval('P' . (Show::DISCOUNT_LAST_DAYS + 1 ) . 'D')
        );

        $result = (new TicketPriceService())->checkPrice($show, $nonDiscountedDate);

        $this->assertEquals($genrePrice, $result);
    }
}
