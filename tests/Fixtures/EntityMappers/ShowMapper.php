<?php
namespace Tickets4Sale\Tests\Fixtures\EntityMappers;

use Ramsey\Uuid\Uuid;
use Tickets4Sale\Domain\Genre\Comedy;
use Tickets4Sale\Domain\Genre\Drama;
use Tickets4Sale\Domain\Genre\Musical;
use Tickets4Sale\Domain\Show\Show;
use TijmenWierenga\Bogus\Generator\MappingFile\AbstractMapper;
use TijmenWierenga\Bogus\Generator\MappingFile\Mappable;
use DateTimeImmutable;
use Faker;

class ShowMapper extends AbstractMapper implements Mappable
{
    /**
     * @return array
     */
    public static function attributes(): array
    {
        // Use Faker for generating dummy data
        $factory = Faker\Factory::create();

        return [
            'id' => Uuid::uuid4(),
            'name' => $factory->name,
            'startDate' => new DateTimeImmutable(),
            'genre' => $factory->randomElement([new Comedy(), new Drama(), new Musical()])

        ];
    }

    /**
     * @param array $attributes
     * @return Show
     */
    public static function create(array $attributes)
    {
        return new Show($attributes['id'], $attributes['name'], $attributes['startDate'], $attributes['genre']);
    }
}
