<?php
namespace Tickets4Sale\Application\Show;

/**
 * Class class ShowInventoryAccessResponse
 *
 * @package Tickets4Sale\Application\Show
 */
class ShowInventoryAccessResponse
{
    /**
     * @var array $inventory
     */
    private $inventory;

    /**
     * InventoryAccessResponse constructor.
     *
     * @param array $inventory
     */
    public function __construct(array $inventory = [])
    {
        $this->inventory = $inventory;
    }

    /**
     * @return array
     */
    public function access(): array
    {
        return $this->inventory;
    }
}
