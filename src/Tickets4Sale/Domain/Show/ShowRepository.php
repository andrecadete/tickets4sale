<?php
namespace Tickets4Sale\Domain\Show;

use ArrayIterator;
use DateTimeInterface;

/**
 * Interface ShowRepository
 * Show-specific Repository method signatures. Better type-hint - therefore validation - than if in parent Interface.
 *
 * @package Tickets4Sale\Infrastructure\Show
 */
interface ShowRepository
{
    /**
     * Saves a Show Entity
     *
     * @param Show $show
     */
    public function save(Show $show): void;

    /**
     * @param string $id
     * @return null|Show
     */
    public function find(string $id):? Show;

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     * @return null|ArrayIterator
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ):? ArrayIterator;

    /**
     * Finds Shows that are running on the given $showDate
     *
     * @param  DateTimeInterface $showDate
     * @return array
     */
    public function findByShowDate(DateTimeInterface $showDate): array;
}
