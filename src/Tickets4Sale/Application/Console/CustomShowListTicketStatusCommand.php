<?php
namespace Tickets4Sale\Application\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Tickets4Sale\Application\Service\ShowInventoryService;
use Tickets4Sale\Application\Show\ShowInventoryStatusResponse;
use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Application\Show\ShowInventoryStatusRequest;
use DateTimeImmutable;
use Tickets4Sale\Infrastructure\Importer\ShowCsvImporter;

/**
 * Class CustomShowListTicketStatusCommand
 *
 * @package Tickets4Sale\Application\Console
 */
class CustomShowListTicketStatusCommand extends ShowListTicketStatusCommand
{
    /**
     * @var DecoderInterface $csvEncoder
     */
    private $decoder;

    /**
     * @var string $basePath
     */
    private $basePath;

    /**
     * @var ShowInventoryService
     */
    protected $inventoryService;

    public function __construct(
        string $basePath,
        ShowInventoryService $inventoryService,
        DecoderInterface $csvDecoder
    ) {
        $this->basePath = $basePath;
        $this->decoder = $csvDecoder;
        $this->inventoryService = $inventoryService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('tickets4sale:custom-inventory:status')
            ->setDescription('Check Inventory Status of a given list of Shows')
            ->setHelp(
                "Receives a list of Shows in CSV format to check against current Ticket availability. \n\r
            Format: [title, opening-day (YYYY-MM-DD), genre]"
            )
            ->addArgument(
                'file-path',
                InputArgument::REQUIRED,
                'Path to input file - relative from root project dir.' .
                'Example: file-path="../externalFolder/file.csv" / file-path="tests/data/someFile.csv"'
            )->addArgument(
                'query-date',
                InputArgument::REQUIRED,
                "The Query date. Used to set at which point in time you'd be requesting this information"
            )->addArgument(
                'show-date',
                InputArgument::REQUIRED,
                "The date of the Shows you're checking the status."
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {

        $inputFilePath = $input->getArgument('file-path');
        $inputQueryDate = $input->getArgument('query-date');
        $queryDate = DateTimeImmutable::createFromFormat('Y-m-d', $inputQueryDate);
        $inputShowDate  = $input->getArgument('show-date');
        $showDate = DateTimeImmutable::createFromFormat('Y-m-d', $inputShowDate);

        $importer = new ShowCsvImporter(
            $this->basePath . '/../' . $inputFilePath,
            $this->decoder,
            $showDate
        );

        $shows = $importer->import();

        if (!count($shows)) {
            $output->writeln(["There are no shows for that day!"]);
        }

        $output->writeln(
            json_encode(
                ['inventory' => $this->inventoryStatus($shows, $queryDate, $showDate)->showInventory()],
                JSON_PRETTY_PRINT
            )
        );
    }
}
