<?php
namespace Tickets4Sale\Infrastructure\Show;

use Tickets4Sale\Domain\Show\Show;
use Tickets4Sale\Domain\Show\ShowRepository;
use Tickets4Sale\Infrastructure\Repository\RepositoryWrapper;
use ArrayIterator;
use DateTimeInterface;

/**
 * Class DoctrineShowRepository
 * Our own implementation of ShowRepository, using external dependency for DBAL through a RepositoryWrapper.
 *
 * @package Tickets4Sale\Infrastructure\Show
 */
class DoctrineShowRepository implements ShowRepository
{
    /**
     * @var RepositoryWrapper
     */
    private $repositoryWrapper;

    /**
     * DoctrineShowRepository constructor.
     *
     * @param RepositoryWrapper $repositoryWrapper
     */
    public function __construct(RepositoryWrapper $repositoryWrapper)
    {
        $this->repositoryWrapper = $repositoryWrapper;
    }

    /**
     * @param Show $show
     */
    public function save(Show $show): void
    {
        $this->repositoryWrapper->save($show);
    }

    /**
     * @param string $id
     * @return null|Show
     */
    public function find(string $id):? Show
    {
        return $this->repositoryWrapper->find($id);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     * @return null|ArrayIterator
     */
    public function findBy(
        array $criteria,
        array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null
    ):? ArrayIterator {
        $records = $this->repositoryWrapper->findBy($criteria, $orderBy, $limit, $offset);
        return $records->getIterator();
    }

    /**
     * @param DateTimeInterface $showDate
     * @return array
     */
    public function findByShowDate(DateTimeInterface $showDate): array
    {
        return $this->repositoryWrapper->findByValueBetweenDateFields(
            $showDate,
            'startDate',
            'endDate',
            0
        );
    }
}
